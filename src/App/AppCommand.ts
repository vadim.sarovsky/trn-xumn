import {TFacadeCommand,updateCoreCommand} from "../Core/CoreCommand";

export const catalogAppCommand = {
};

export const facadeAppCommand = {
} as TFacadeCommand;

export const launchAppCommand = () => updateCoreCommand(facadeAppCommand);