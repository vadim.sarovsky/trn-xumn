import React from 'react';
import {Provider} from "mobx-react";
import {observerBatching} from "mobx-react-lite"
import {store} from "../Core/CoreStore";
import {AppConfigs} from "./AppConfigs";
import {launchAppRequest} from "./AppRequest";
import {launchAppCommand} from "./AppCommand";
import {launchAppUI} from "./AppUI";

import {TemplateProvider} from "../Core/UI/Provider/Template/TemplateProvider";

const App = () => {
    observerBatching();
    launchAppRequest();
    launchAppCommand();
    launchAppUI();
    return (
        <Provider store={store}>
            <TemplateProvider templateIdentifier={AppConfigs.rootTemplate}/>
        </Provider>
    );
};

export default App;
