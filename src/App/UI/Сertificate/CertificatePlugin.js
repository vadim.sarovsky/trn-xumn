
import getCadespluginAPI from "async-cadesplugin";


export const requestAvailableCertificate = async () => {

    try {
        const api = await getCadespluginAPI();

        const certificate = await api.getCertsList();
        // const certificate = await api.getValidCertificates(); await api.getCertsList();
        const list = [];
        for( const e of certificate ){
            const property = e.subjectInfo.split(',');
            e["person"] = {};

            for( const name of property ){

                if( name.includes('CN=')){
                    e['CN'] = name.replace('CN=','');
                }
                if( name.includes('SN=')){
                    e['SN'] = name.replace(' SN=','');
                    e["person"]['familyName'] = name.replace(' SN=','');
                }

                if( name.includes('G=')){
                    const t = name.replace(' G=',"").split(" ");
                    e["person"]['patronymic'] = t.pop();
                    e["person"]['firstName'] = t.pop();
                    e['G'] = name.replace(' G=',"");

                }

            }
            if( e.hasOwnProperty('SN')){
                list.push( e );
            }
        }
        return list;
    } catch (error) {
        return undefined
    }

};

export const SignFile = async (data, thumbprint) => {
    const api = await getCadespluginAPI();
    data.name = data.name.replace('.xml','.bin');
    data.content = await api.signBase64( thumbprint, String(data.content) );
    return data;
};