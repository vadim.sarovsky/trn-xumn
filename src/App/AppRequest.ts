import {AppConfigs} from "./AppConfigs";
import {TFacadeRequest, launchCoreRequest} from "../Core/CoreRequest";

export const catalogAppRequest = {
    templateRequest: () => {
        const URI = (templateIdentifier:string) => AppConfigs.API.template + templateIdentifier;
        const mimeType:string = "application/xml";
        const description = () => {
            return{
                method: "GET",
                headers: {
                    "Content-Type": mimeType,
                }
            }
        };
        return {
            URI:(templateIdentifier:string) => URI(templateIdentifier),
            description:() => description(),
            mimeType
        };
    },
};

export const facadeAppRequest = {
    "templateRequest": catalogAppRequest.templateRequest,
} as TFacadeRequest;

export const launchAppRequest = () => launchCoreRequest(facadeAppRequest);