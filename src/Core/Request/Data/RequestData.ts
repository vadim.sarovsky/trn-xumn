import {action} from "mobx";
import {RequestDataType} from "./RequestDataType";

const message: { [id: string]: any } = {
    400:{code:"400"},
    401:{code:"401"},
    403:{code:"403"},
    404:{code:"404"},
    409:{code:"409"},
    500:{code:"500"},
    disconnect:{code:"disconnect"},

};

export class RequestData {
    public status: "process" | "success" | "failure";
    public response: any;
    constructor() {
        this.status = "process";
        this.response = null;
    }
    protected fetchData = async (request:any) => {
        const controller = new AbortController();
        setTimeout(() => controller.abort(), 1000);
        request.signal = controller.signal;
        try {
            const response = await fetch(request.URI, request.description);
            switch ( response.status ) {
                case 200:
                    return await response.json();
                case 400:
                    return {error:message[400]};
                case 401:
                    return {error:message[401]};
                case 403:
                    return {error:message[403]};
                case 404:
                    return {error:message[404]};
                case 409:
                    return {error:message[409]};
                case 500:
                    return {error:message[500]};
                default:
                    return {error:"unknown"};
            }
        } catch (error) {
            return {error: message["disconnect"]};
        }
    };

    @action public obtain = async (request: RequestDataType) => {
        if( request ){
            const response:any = await this.fetchData(request);
            if( response.error ){
                this.status = "failure";
                this.response = response.error;
            } else {
                this.status = "success";
                this.response = response;
            }
        } else {
            this.status = "failure";
        }
    }
}