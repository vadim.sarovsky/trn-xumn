import {observable,action} from "mobx";
import {RequestData} from "../../Request/Data/RequestData";
import {CoreBinding} from "../../CoreBinding";
import {BindingParser} from "../../Util/BindingParser";
import {requestParser} from "../../Factory/RequestFactory";
import {CoreConverter} from "../../CoreConverter";
import {TContentService} from "./TContentService";
import {store} from "../../CoreStore";
import {collectChildrenAttr} from "../../Util/NodeXML";
import {conditionFacade} from "../../CoreCondition";

export class ContentService {
    @observable public state: boolean;
    protected service:RequestData;
    constructor() {
        this.state = false;
        this.service = new RequestData();
    }
    sleep = (m:number) => new Promise(r => setTimeout(r, m));

    @action process = async (props: TContentService) => {
        if(props.processNotice){
            store.process.launch({notice:props.processNotice})
        }
        const requestData = requestParser.launch(props.template);
        const requestProp = {
            URI:requestData.URI(),
            description:requestData.description(),
            processNotice:{
                name: props.processNotice,
                notification: props.processNotice
            }
        };
        await this.service.obtain(requestProp);
        switch ( this.service.status ) {
            case "success":
                const into = BindingParser.bindingParsingExpression(requestData.binding());
                into.bindingValue = this.service.response;
                new CoreConverter().launch(into);
                new CoreBinding().sourceValue(into.bindingSource).method.record( into );
                this.state = true;
                store.process.method.finish();
                break;
            case "process":
                store.process.method.finish();
                break;
            case "failure":
                store.process.method.finish();
                store.failure.method.launch(this.service.response);
                break;
            default:
                store.process.method.finish();
                return {error:"unknown"};
        }
    };


    @action asynchronousProcess = async (props: TContentService) => {
        const requestData = requestParser.launch(props.template);
        const requestProp = {
            URI:requestData.URI(),
            description:requestData.description(),
            processNotice:{
                name: props.processNotice,
                notification: props.processNotice
            }
        };
        await this.service.obtain(requestProp);

        switch ( this.service.status ) {
            case "success":
                const binding = BindingParser.bindingParsingExpression(requestData.binding());
                binding.bindingValue = this.service.response;
                new CoreConverter().launch(binding);
                new CoreBinding().sourceValue(binding.bindingSource).method.record( binding );
                const condition = {status: true } as { [key:string]: boolean };
                collectChildrenAttr(props.template,":root > Request > Condition").map( (props:any) => condition.status = conditionFacade(props) ?  condition.status : false );
                if( condition.status ){
                    this.state = true;
                } else {
                    await this.sleep( 500 );
                    await this.asynchronousProcess(props);
                }
                break;
            case "process":
                break;
            case "failure":
                store.failure.method.launch(this.service.response);
                break;
            default:
                return {error:"unknown"};
        }





    };

    @action public supplyContent = async (props: TContentService) => {
        this.state = false;
        const binding = requestParser.launch(props.template).binding();
        if(props.providerType === "Regular"){
            new CoreBinding().obtainValue(binding).bindingValue ? this.state = true : await this.process(props);
        } else if(props.providerType === "Asynchronous"){
            await this.asynchronousProcess(props);
        } else {
            await this.process(props);
        }
    }
}

export const contentService:ContentService = new ContentService();