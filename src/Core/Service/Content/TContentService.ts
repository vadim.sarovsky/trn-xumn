export type TContentService = {
    into:string;
    providerType:string;
    template:string;
    processNotice?: string;
    successNotice?: string;
    failureNotice?: string;
}