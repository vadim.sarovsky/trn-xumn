import {store} from "../../CoreStore";
import {obtainCoreRequest} from "../../CoreRequest";
import {AppConfigs} from "../../../App/AppConfigs";
import {TemplateRequest} from "../../Request/Template/TemplateRequest";

export class TemplateService {
    public status: "process" | "success" | "failure";
    private service:  TemplateRequest;

    constructor() {
        this.service = new TemplateRequest();
        this.status = "process";
    }

    public launch = async (param:any) => {
        if(store.template.method.obtain(param.templateIdentifier)){
            this.status = "success";
        } else {
            if(param.processNotice){
                store.process.launch({notice:param.processNotice})
            }
            const request = obtainCoreRequest(AppConfigs.templateRequest);
            if(request){
                try {
                    const requestWithParam = {
                        URI:request.URI(param.templateIdentifier),
                        description:request.description,
                        mimeType:request.mimeType,
                    };
                    await this.service.obtain(requestWithParam);
                    if(this.service.status === "success"){
                        if(this.service.response){
                            const template = this.service.response.replace(/>\s+?</g, "><").replace(/\s+/g, ' ');
                            store.template.method.record({id: param.templateIdentifier, template: template});
                            this.status = "success";
                            store.process.method.finish();
                        }
                    }
                } catch ( error ) {
                    store.process.method.finish();
                    this.status = "failure";
                }
            } else {
                store.process.method.finish();
                this.status = "failure";
            }
        }
    }
}