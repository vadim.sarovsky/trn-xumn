import {observable,action} from "mobx";
import {RequestData} from "../../Request/Data/RequestData";
import {CoreBinding} from "../../CoreBinding";
import {requestParser} from "../../Factory/RequestFactory";
import {TCommandService} from "./TCommandService";
import {store} from "../../CoreStore";

export class CommandService {
    @observable public state: boolean;
    protected service:RequestData;
    constructor() {
        this.state = false;
        this.service = new RequestData();
    }
    @action public launch = async (props: TCommandService) => {
        if(props.processNotice){
            store.process.launch({notice:props.processNotice})
        }
        const requestData = requestParser.launch(props.template);

        const binding = requestData.binding();

        const requestProp = {
            URI:requestData.URI(),
            description:requestData.description(),
            processNotice:{
                name: props.processNotice,
                notification: props.processNotice
            }
        };
        await this.service.obtain(requestProp);
        switch ( this.service.status ) {
            case "success":
                store.process.method.finish();
                if(binding){
                    new CoreBinding().recordValue(binding,this.service.response);
                }
                return this.service.response;
            case "process":
                store.process.method.finish();
                break;
            case "failure":
                store.process.method.finish();
                store.failure.method.launch(this.service.response);
                break;
            default:
                store.process.method.finish();
                return {error:"unknown"};
        }
    };
}

export const commandService:CommandService = new CommandService();