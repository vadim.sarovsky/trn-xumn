import {BindingParser} from "./Util/BindingParser";
import {CoreBinding} from "./CoreBinding";

export const notEmptyAll = (value:string) => {
    const data = new CoreBinding().obtainValue(value).bindingValue;
    return Array.isArray(data) ? data.length !== 0 : false;
};

export const emptyAll = (value:string) => {
    const data = new CoreBinding().obtainValue(value).bindingValue;
    return Array.isArray(data) ? data.length === 0 : true
};

export const hasAllOutput = (value:string) => {
    let result:boolean = false;
    BindingParser.bindingCollection(value).map(e => new CoreBinding().outputValue(e).bindingValue === "" ? result = true : null );
    return result;
};

export const notHasAllOutput = (value:string) => {
    let result:boolean = false;
    BindingParser.bindingCollection(value).map(e => new CoreBinding().outputValue(e).bindingValue === "" ? result = true : null );
    return result;
};

export const hasAll = (value:string) => {
    let result:boolean = true;
    BindingParser.bindingCollection(value).map(e => new CoreBinding().obtainValue(e).bindingValue === undefined ? result = false : null );
    return result;
};

export const hasAny = (value:string) => {
    let result:boolean = false;
    BindingParser.bindingCollection(value).map(e => new CoreBinding().obtainValue(e).bindingValue !== undefined ? result = true : null );
    return result;
};

export const notHasAny = (value:string) => {
    let result:boolean = false;
    BindingParser.bindingCollection(value).map(e => new CoreBinding().obtainValue(e).bindingValue === undefined ? result = true : null );
    return result;
};

export const notHasAll = (value:string) => {
    let result:boolean = true;
    BindingParser.bindingCollection(value).map(e => new CoreBinding().obtainValue(e).bindingValue !== undefined ? result = false : null);
    return result;
};



export const hasOnly = (value:string, source:string) => {
    const refers = new CoreBinding().refersValue(source);
    if(typeof refers === "object"){
        let result:boolean = true;
        const collection: {[key:string]:any} = {};
        BindingParser.bindingCollection(value).map(e => collection[new CoreBinding().obtainValue(e).bindingPath] = new CoreBinding().obtainValue(e).bindingValue);
        Object.keys(refers).map(key=> collection.hasOwnProperty(key) ?  null : result = false);
        return result;
    } else {
        return false
    }
};

export const notHasOnly = (value:string, source:string) => {
    const refers = new CoreBinding().refersValue(source);
    if(typeof refers === "object"){
        let result:boolean = false;
        const collection: {[key:string]:any} = {};
        BindingParser.bindingCollection(value).map(e => collection[new CoreBinding().obtainValue(e).bindingPath] = new CoreBinding().obtainValue(e).bindingValue);
        Object.keys(refers).map(key=> collection.hasOwnProperty(key) ?  null : result = true);
        return result;
    } else {
        return false
    }
};

export const equalAll = (value:string) => {
    let result:boolean = true;
    const collection: Array<any> = [];
    BindingParser.bindingCollection(value).map(e => collection.push(new CoreBinding().obtainValue(e).bindingValue));
    collection.map(e => e !== collection[0] ? result = false : null);
    return result;
};

export const notEqualAll = (value:string) => {
    let result:boolean = true;
    const collection: Array<any> = [];
    BindingParser.bindingCollection(value).map(e => collection.push(new CoreBinding().obtainValue(e).bindingValue));
    collection.map(e => e === collection[0] ? result = false : null);
    return result;
};

export type TCondition = {
    conditionType:string;
    value:string;
    source:string;
    valueType:string;
}

export const conditionCatalog:{[key:string]:Function} = {
    "NotHasAllOutput": (param:TCondition) => notHasAllOutput(param.value),
    "HasAll": (param:TCondition) => hasAll(param.value),
    "HasAny": (param:TCondition) => hasAny(param.value),
    "NotHasAny": (param:TCondition) => notHasAny(param.value),
    "HasOnly": (param:TCondition) => hasOnly(param.value,param.source),
    "NotHasAll": (param:TCondition) => notHasAll(param.value),
    "NotHasOnly": (param:TCondition) => notHasOnly(param.value,param.source),
    "EmptyAll": (param:TCondition) => emptyAll(param.value),
    "NotEmptyAll": (param:TCondition) => notEmptyAll(param.value),
    "EqualAll": (param:TCondition) => equalAll(param.value),
    "NotEqualAll": (param:TCondition) => notEqualAll(param.value),
};

export const conditionFacade = (param:TCondition) => conditionCatalog.hasOwnProperty(param.conditionType) ? conditionCatalog[param.conditionType](param) : false;

