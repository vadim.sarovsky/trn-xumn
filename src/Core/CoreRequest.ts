export type TFacadeRequest = { [name:string]:Function };

export const facadeCoreRequest = {} as TFacadeRequest;

export const launchCoreRequest = (facadeAppRequest: TFacadeRequest ) => {
    for(const key in facadeAppRequest ){
        if(facadeAppRequest.hasOwnProperty(key)){
            facadeCoreRequest[key] = facadeAppRequest[key];
        }
    }
};

export const obtainCoreRequest = (requestName: string) => {
    return facadeCoreRequest.hasOwnProperty(requestName) ? facadeCoreRequest[requestName]() : null;
};