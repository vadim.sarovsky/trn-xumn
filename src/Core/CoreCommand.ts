import {store} from "./CoreStore";
import {CoreBinding} from "./CoreBinding";
import {replaceHistory,installHistory} from "./UI/Router/RouterCommand";
import {ContentService} from "./Service/Content/ContentService";
import {collectChildrenAttr} from "./Util/NodeXML";
import {linkGenerator} from "./Util/LinkGenerator";
import {downloadFromData} from "./Command/DownloadFromData";
import {asyncDownload} from "./Command/AsyncDownload";

export type TFacadeCommand = { [name:string]:Function };

export const catalogCoreCommand = {
    authorization: () =>{
        return {
            perform:() => store.session.method.launch(),
        };
    },
    logout: () =>{
        return {
            perform:() => {
                store.content.method.revoke();
                store.session.method.revoke();
            },
        };
    },
    request: () => {
        return {
            perform:(props:any) => {
                return new ContentService().supplyContent(props)
            }
        };
    },
    recordValue: () => {
        return {
            perform:(props:any) => new CoreBinding().recordValue(props.into,props.value)
        }
    },
    removeValue: () => {
        return {
            perform:(props:any) => new CoreBinding().removeValue(props.into)
        }
    },
    recordSetterValue: () => {
        return {
            perform:(props:any) => collectChildrenAttr(props.template, ":root > Setter").map( (e:any) => new CoreBinding().recordValue(e.into,e.value))
        }
    },
    recordCollection: () => {
        return {
            perform:(props:any) => {
                collectChildrenAttr(props.template, ":root > Setter").map( (e:any) => {
                    const value = {} as { [name:string]:any };
                    Object.keys(e).map( key => value[key] = e[key]);
                    return new CoreBinding().recordValue(props.into,value);
                })
            }
        }
    },
//TODO:изменить генератор на props
    //генератор должен сам определять setter или нет
    changeParam: () => {
        return {
            perform:(props:any) => replaceHistory(linkGenerator(props.template,props.path,props.valueType))
        }
    },

    recordParam: () => {
        return {
            perform:(props:any) => {
                const binding = new CoreBinding().obtainValue(props.value);
                let value = binding.bindingValue;
                value = typeof value === "string" ? value : JSON.stringify(value);
                const URI =  new URL(window.location.href);
                URI.searchParams.set(props.param,value);
                window.history.replaceState("","",URI.href);
            }
        }
    },
    recordBasePage: () => {
        return {
            perform:(props:any) => {
                const URI =  new URL(window.location.href);
                URI.searchParams.set(props.param,props.value);
                installHistory(URI.href)
            }
        }
    },
    recordBaseRows: () => {
        return {
            perform:(props:any) => {
                const paging: {[name:string]:number} = {
                    page: new CoreBinding().obtainValue(props.page).bindingValue,
                    rows: new CoreBinding().obtainValue(props.rows).bindingValue,
                    count: new CoreBinding().obtainValue(props.count).bindingValue
                };
                paging.total = Math.ceil(paging.count/props.value);
                paging.total = --paging.total;
                const URI =  new URL(window.location.href);
                if(paging.page > paging.total ){
                    URI.searchParams.set("feed.paging.page",String(paging.total));
                }
                URI.searchParams.set(props.param,props.value);
                installHistory(URI.href)
            }
        }
    },
    recordPrevPage: () => {
        return {
            perform:(props:any) => {
                const paging: {[name:string]:number} = {
                    page: new CoreBinding().obtainValue(props.page).bindingValue,
                };
                paging.page = --paging.page >= 0 ? paging.page : ++paging.page;
                const URI =  new URL(window.location.href);
                URI.searchParams.set(props.param,String(paging.page));
                installHistory(URI.href)
            }
        }
    },
    recordNextPage: () => {
        return {
            perform:(props:any) => {
                const paging: {[name:string]:number} = {
                    page: new CoreBinding().obtainValue(props.page).bindingValue,
                    rows: new CoreBinding().obtainValue(props.rows).bindingValue,
                    count: new CoreBinding().obtainValue(props.count).bindingValue
                };
                paging.page = ++paging.page < Math.ceil(paging.count/paging.rows ) ? paging.page : --paging.page;
                const URI =  new URL(window.location.href);
                URI.searchParams.set(props.param,String(paging.page));
                installHistory(URI.href)
            }
        }
    },
    redirect: () => {
        const perform = async (props:any) => {
            if(props.delayTime){
                const sleep = (m:number) => new Promise(r => setTimeout(r, m));
                await sleep( Number(props.delayTime) );
            }
            replaceHistory( window.location.origin + props.path );
        };
        return {
            perform:(props:any ) => perform(props)
        }
    },
    back: () => {
        return {
            perform:() => window.history.back()
        }
    },
    launchModal: () => {
        return {
            perform:(props:any) => store.modal.method.launch(props)
        }
    },
    finishModal: () => {
        return {
            perform:() => store.modal.method.finish()
        }
    },
    launchFilters: () => {
        return {
            perform:(props:any) => {
                const value = new CoreBinding().refersValue(props.value);
                const URI =  new URL(window.location.href);
                Object.keys(value).map( (key:string) =>
                    Array.isArray(value[key]) ? URI.searchParams.set(key,JSON.stringify(value[key])) : URI.searchParams.set(key,value[key]));
                store.modal.method.finish();
                store.filters.method.revoke();
                installHistory( URI.href);
            }
        }
    },
    revokeFilters: () => {
        return {
            perform:() => store.filters.method.revoke()
        }
    },
    downloadFromData: () => {
        return {
            perform:(props:any) => downloadFromData(props)
        }
    },
    download: () => {
        return {
            perform:(props:any) => asyncDownload(props)
        }
    },
};

export const facadeCoreCommand = {
    "Authorization": catalogCoreCommand.authorization,
    "Logout": catalogCoreCommand.logout,
    "RecordValue": catalogCoreCommand.recordValue,
    "RemoveValue": catalogCoreCommand.removeValue,
    "RecordSetterValue": catalogCoreCommand.recordSetterValue,
    "RecordCollection":catalogCoreCommand.recordCollection,
    "Request": catalogCoreCommand.request,
    "Redirect": catalogCoreCommand.redirect,

    "Back": catalogCoreCommand.back,
    "RecordParam": catalogCoreCommand.recordParam,
    "RecordBaseRows": catalogCoreCommand.recordBaseRows,
    "RecordBasePage": catalogCoreCommand.recordBasePage,
    "RecordPrevPage": catalogCoreCommand.recordPrevPage,
    "RecordNextPage": catalogCoreCommand.recordNextPage,
    "ChangeParam":catalogCoreCommand.changeParam,
    "LaunchModal": catalogCoreCommand.launchModal,
    "FinishModal": catalogCoreCommand.finishModal,
    "LaunchFilters": catalogCoreCommand.launchFilters,
    "RevokeFilters": catalogCoreCommand.revokeFilters,
    "DownloadFromData": catalogCoreCommand.downloadFromData,
    "Download": catalogCoreCommand.download,
} as TFacadeCommand;

export const updateCoreCommand = ( facadeAppCommand: TFacadeCommand ) => {
    for(const key in facadeAppCommand ){
        if(facadeAppCommand.hasOwnProperty(key)){
            facadeCoreCommand[key] = facadeAppCommand[key];
        }
    }
};

export const obtainCoreCommand = (command: string) => {
    return facadeCoreCommand.hasOwnProperty(command) ? facadeCoreCommand[command]() : undefined;
};