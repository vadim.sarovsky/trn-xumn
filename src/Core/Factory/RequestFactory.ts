import {parseDocumentElement} from "../Util/NodeXML";
import {CoreBinding} from "../CoreBinding";
import {AppConfigs} from "../../App/AppConfigs";

class RequestFactory {
    elementChildren = (o:any,rootElement:Element|null) => {
        if(rootElement){
            let i:number = 0;
            const key = rootElement.getAttribute("targetName");
            if(key){
                const value = rootElement.getAttribute("value");
                if(value){
                    o[key] = new CoreBinding().obtainValue(value).bindingValue;
                } else {
                    o[key] = {};
                    let n = o[key];
                    while( i < rootElement.children.length){
                        const element = rootElement.children[i];
                        if( element ){
                            this.elementChildren(n,element)
                        }
                        i++
                    }
                }
            } else {
                console.log("Incorrect request targetName ")
            }
        }
        return o
    };

    select = (element:Element|null) => {
        let o = {};
        let i:number = 0;
        if(element){
            const value = element.getAttribute("value");
            if(value){
                o = new CoreBinding().obtainValue(value).bindingValue
            } else {
                while( i < element.children.length){
                    this.elementChildren(o,element.children[i++]);
                }
            }
        }
        return o
    };

    description = (template:string) => {

        const element = parseDocumentElement(template).querySelector(":root > Request");

        if(element){
            return {
                method: element.getAttribute("method"),
                headers: this.select( element.querySelector("Headers")),
                body: JSON.stringify(this.select( element.querySelector("Body")))
            }
        } else {
            return null;
        }
    };
    URI = (template:string) => {
        const element = parseDocumentElement(template).querySelector(":root > Request");
        return element ? AppConfigs.API.origin + element.getAttribute("API") : "";
    };
    binding = (template:string) =>{
        const binding = parseDocumentElement(template)?.querySelector(":root > Request")?.getAttribute("binding");
        return binding ? binding : ""
    };

    launch = (template:string) => {
        return {
            URI: () => this.URI(template),
            binding:() => this.binding(template),
            description:() => this.description( template )
        };
    }
}

export const requestParser = new RequestFactory();