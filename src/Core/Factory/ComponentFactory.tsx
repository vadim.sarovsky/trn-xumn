import React, { FunctionComponent } from "react";
import {useObserver} from "mobx-react-lite";
import {parseDocumentElement,cloneElement,collectElementAttr} from "../Util/NodeXML";
import {CoreUI} from "../CoreUI";

type ComponentFactoryType ={
    template:string;
}

export const ComponentFactory: FunctionComponent<ComponentFactoryType> = (props) => {
    const elementChildren = (rootElement:Element)=> {
        let children = [],
            i:number = 0,
            key:number = 0;
        while( i < rootElement.children.length){
            const element = rootElement.children[i];
            if( element ){
                children.push(elementFactoryParse(element, ++key))
            }
            i++
        }
        return children;
    };
    const elementProps = (element:Element, key:number) =>{
        const property = collectElementAttr(element);
        property["key"] = key;
        property["template"] = cloneElement(element);
        property["children"] = elementChildren(element);
        return property;
    };
    const elementFactoryParse = (element:Element, key:number) => {
        const componentImport = CoreUI[element.tagName];
        if (componentImport?.default) {
            const Component = componentImport.default;
            const props = elementProps( element, key );
            return <Component {...props}/>;
        } else {
            return null;
        }
    };
    const {template} = props;
    const element = parseDocumentElement(template);
    return useObserver(() => elementFactoryParse(element, 0));
};