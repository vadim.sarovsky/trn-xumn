import {styleDictionary,triggerDefinition} from "../UI/StyleDefinition";
import {parseDocumentElement} from "../Util/NodeXML";

type TStyleFactory = {
    template:string;
}

export class ResourceFactory {
    protected style:string;
    protected collection:{[key:string]:any};
    constructor() {
        this.style = "";
        this.collection = {};
    }

    propertyParser = (o:any) =>{
        let result = "";
        Object.keys(o).map(key => result += key + ":" + o[key] + ";");
        return "{" + result + "}";
    };

    targetParser = (root:Element, targetType:string, targetName:string|null, targetTrigger:string|null) => {
        const node = root.querySelectorAll("Setter");
        const result:{[key:string]:string|null} = {};
        let i = 0;
        while (i < node.length){
            const property = node[i].getAttribute("Property");
            const value = node[i].getAttribute("Value");
            if(property) {
                if(styleDictionary[targetType]){
                    styleDictionary[targetType][property] ? result[styleDictionary[targetType][property]] = value : console.log("Incorrect style property",targetType,targetName,property);
                } else {
                    console.log("Incorrect style targetType", targetType)
                }
            }
            i++
        }
        if(targetName){
            let name = "";

            if( targetType=== "StatusCode"){
                name = `[data-status-code="${targetName}"]`;

            } else {
                name = `[data-style="${targetName}"]`;

            }
            if(targetTrigger){
                if(triggerDefinition[targetTrigger]){
                    name = name + triggerDefinition[targetTrigger];

                }
            }
            this.collection[name] = result;
        } else {
            console.log("Incorrect target name", targetName);
        }
    };

    collectorStyle = () =>
        Object.keys(this.collection).map(key => this.style += key + this.propertyParser(this.collection[key]));

    launch = (props:TStyleFactory) => {
        this.style = "";
        const root = parseDocumentElement(props.template);
        const node = root.querySelectorAll("Style");
        let i = node.length;
        while (i--){
            const targetType = node[i].getAttribute("Type");
            const targetName = node[i].getAttribute("Name");
            const targetTrigger = node[i].getAttribute("Trigger");
            targetType ? this.targetParser(node[i], targetType, targetName, targetTrigger) : console.log("Incorrect style type");
        }
        this.collectorStyle();
        return this.style
    }

}