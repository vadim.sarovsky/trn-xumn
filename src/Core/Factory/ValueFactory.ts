import {parseDocumentElement} from "../Util/NodeXML";
import {CoreBinding} from "../CoreBinding";
import {BindingParser} from "../Util/BindingParser";

class ValueFactory {
    elementChildren = (o:any,rootElement:Element|null) => {
        if(rootElement){
            let i:number = 0;
            const key = rootElement.getAttribute("targetName");
            if(key){
                const value = rootElement.getAttribute("value");
                const bindingValue = rootElement.getAttribute("BindingValue");


                if(value) {
                    o[key] = new CoreBinding().obtainValue(value).bindingValue;
                } else if(bindingValue ){
                    o[key] = new CoreBinding().obtainValue(bindingValue).bindingValue;

                } else {
                    o[key] = {};
                    let n = o[key];
                    while( i < rootElement.children.length){
                        const element = rootElement.children[i];
                        if( element ){
                            this.elementChildren(n,element)
                        }
                        i++
                    }
                }
            } else {
                console.log("Incorrect targetName ")
            }
        }
        return o
    };



    launch = (template:string) => {
        const element = parseDocumentElement(template);
        let result:{[key:string]:any} = {};
        let i:number = 0;
        const value = element.getAttribute("value");
        if(value){
            result = new CoreBinding().obtainValue(value).bindingValue;
        } else {
            while( i < element.children.length){
                this.elementChildren(result,element.children[i++]);
            }
        }
        return result;
    }
}

export const valueFactory = new ValueFactory();