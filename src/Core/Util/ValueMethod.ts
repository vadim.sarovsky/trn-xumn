export const obtainValue = (o: any, path:string,) => {

    const prop = path?.split('.');
    if(Array.isArray(prop)){
        for (let i = 0; i < prop.length; i++) {
            const key = prop[i];
            if (Object.isExtensible(o)) {
                if (i + 1 === prop.length) {
                    return o[key];
                } else {
                    o = o[key];
                }
            }
        }
    } else {
        console.log( "Incorrect binding path:", path );
        return null;
    }
};

type TRecordValue = {
    bindingPath:string;
    bindingValue:any;
    recordProperty?:string;
}

export const recordValue = (data: any, param:TRecordValue ) => {

    const prop:Array<string> = param.bindingPath.split(".");
    prop?.map( (key, i) => {
        if(++i === prop.length) {
            if(param.recordProperty === "[]"){
                data[key] = Array.isArray(data[key]) ? data[key] : [];
            }
            Array.isArray(data[key]) ? data[key].push(param.bindingValue) : data[key] = param.bindingValue;
        }
        data[key] = data.hasOwnProperty(key) ? data[key] : {};
        data = data[key];
    });
};


export const removeValue = (o: any, path:string, value?:any ) =>{
    const prop:Array<string> = path.split(".");
    prop?.map( (e, i) => {
        if(++i === prop.length){
            if(Array.isArray(o[e])){
                if(value){
                    const index = o[e].indexOf(value, 0);
                    if (index >= 0) {
                        o[e].splice(index, 1);
                    }
                } else {
                    delete o[e]
                }
            } else {
                delete o[e]
            }
        } else {
            if(o.hasOwnProperty(e)){
                o = o[e];
            }
        }
    });
};

export const refersValue = ( bindingValue: any, bindingPath:string ) =>{
    const result:{[key:string]:any} = {};
    const value = obtainValue(bindingValue, bindingPath);
    const getReference = (o:any, path?:string) => {
        for(const key in o){
            if(o.hasOwnProperty(key)){
                if(typeof o[key] === "object"){
                    /*
                    if(Array.isArray(o[key])){
                        o[key].map((e:any,i:number)=>{
                            console.log( key )
                            console.log( i )
                            console.log( "e", e )

                            result[path + "." + key + "." + i] = e
                        })
                    } else {
                        path ? getReference(o[key], path + "." + key): getReference(o[key], key);

                    }

                     */
                    path ? getReference(o[key], path + "." + key): getReference(o[key], key);

                } else {
                    if(path){
                        if(Array.isArray(o)){
                            result[path] = [];
                            o.map(e => result[path].push( e ));
                        } else {
                            result[path + "." + key] = o[key];
                        }
                    } else {
                        result[key] = o[key];
                    }
                }
            }
        }
    };
    getReference(value,bindingPath);
    return result;
};