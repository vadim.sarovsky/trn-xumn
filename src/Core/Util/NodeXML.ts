import {CoreBinding} from "../CoreBinding";

export const parseXML = ( template:string ) => new DOMParser().parseFromString(template, "text/xml");

export const parseDocumentElement = ( template:string ) => parseXML(template).documentElement;

export const cloneElement = ( element:Element  ) => new XMLSerializer().serializeToString(element.cloneNode(true));

export const cloneDocumentElement = ( element:Element ) => parseXML(new XMLSerializer().serializeToString(element)).documentElement;

export const fragment = ( pattern:string ) => {
    const template = parseXML(pattern);
    template.documentElement.removeAttribute("type");
    return template.documentElement;
};

export const replaceRootElement= ( template:string, templateFragment:string ) => {
    const fragment = parseXML(templateFragment).documentElement;
    const children = parseXML(template).documentElement.children;
    let i = children.length;
    while (i--){
        fragment.prepend(children[i])
    }
    return fragment;
};

export const collectElementAttr = (element:Element) => {
    let prop: { [name: string]: any } = {},
        name: string,
        value: any,
        elementAttribute: any = element.attributes,
        i: number = elementAttribute.length;
    while (i--) {
        name = elementAttribute[i].name;
        value = elementAttribute[i].value;
        prop[name] = value;
    }
    return prop;
};

export const collectChildrenAttr = ( template:string, selector:string )=> {
    const prop:any[] = [];
    const XML = parseXML(template);
    const element = XML.querySelectorAll( selector);
    let i = 0;
    while (i < element.length){
        const elementProps = collectElementAttr(element[i]);
        prop.push(elementProps);
        i++
    }
    return prop;
};

const allSetter = (root:Element, elementName:string, data:any  ) => {
    const props = collectElementAttr(root);
    if( root.tagName === elementName){
        if(props.targetName){
            if(props.value){
                data[props.targetName] = new CoreBinding().obtainValue(props.value).bindingValue;
            } else {
                data[props.targetName] = {};
                let i = root.children.length;
                while (i--) {
                    const element = root.children[i];
                    if( element ){
                        allSetter(root.children[i], elementName, data[props.targetName]);
                    }
                }
            }
        }
    }
};

export const collectAllChildrenAttr = ( template:string, selector:string )=> {
    const result:any = {};
    const element = parseXML(template).documentElement.children;
    let i = element.length;
    while (i--){
        allSetter( element[i],"Setter",result);
    }
    return result;
};

export const recordElementValueAttr = ( bindingSource:string, bindingPath:string, bindingIndex:number, node:Element) => {
    let element = node.querySelectorAll("[value]");
    let i = element.length;
    while (i--){
        let value = element[i].getAttribute("value");
        if(value?.includes("Binding Path=")){
            const currentPath = `Path=${bindingPath}.${bindingIndex}.`;
            value = value.replace("Binding ", "Binding Source=" + bindingSource + "," );
            value = value.replace("Path=", currentPath );
            element[i].setAttribute("value",value);
        }
    }
};
