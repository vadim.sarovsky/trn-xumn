import {collectChildrenAttr} from "./NodeXML";
import {CoreBinding} from "../CoreBinding";

type TSetter = {
    value:string,
    param:string
}

export const linkGenerator = (template:string,pathname:string,valueType?:string) => {
    const URI =  new URL(window.location.origin + pathname);
    if(valueType === "Setter") {
        const setter = collectChildrenAttr(template,":root > Setter");
        setter?.map((e:TSetter) => {
            const param = e.param;
            let value = new CoreBinding().obtainValue(e.value ).bindingValue;
            value = typeof value === "string"? value : JSON.stringify(value);
            URI.searchParams.set(param,value);
        })
    }
    if(valueType === "ValueSetter") {
        /*
        import {collectAllChildrenAttr} from "./NodeXML";

        const data = collectAllChildrenAttr(template,":root > Setter");
        Object.keys(data).map( key => {
            const value = typeof data[key] === "string"? data[key] : JSON.stringify(data[key]);
            return URI.searchParams.set(key,value);
        })

         */
    }
    return URI.href;
};