import {store} from "./CoreStore";
import {BindingParser} from "./Util/BindingParser";
import {CoreConverter} from "./CoreConverter";
import {CoreConstruct} from "./CoreConstruct";

export class CoreBinding {
    // Список доступных сторов со обладающих стандартными методами
    source = {
        "Content": store.content,
        "Session": store.session,
        "Failure": store.failure,
        "Process": store.process,
        "Filters": store.filters,
        "Location": store.location,
        "Dictionary": store.dictionary,
    } as { [key:string]:any };

    sourceValue = (bindingSource:string|undefined) =>{
        if(bindingSource){
            return this.source[bindingSource] ? this.source[bindingSource] : undefined;
        } else {
            console.log("Incorrect remove binding:",bindingSource)
        }
    };

    obtainValue = (expression:string) => {
        const binding = BindingParser.bindingParsingValue(expression);
        // Проверка запрос данных из стора или текущее значение
        if(binding.bindingSource){
            binding.bindingValue = this.sourceValue(binding.bindingSource).method.obtain( binding );
        }
        // Проверка на наличие конструирования
        new CoreConstruct().launch(binding);
        // Проверка на наличие конвертации
        new CoreConverter().launch(binding);
        return binding;
    };

    refersValue = (expression:string)=>{
        const binding = BindingParser.bindingParsingValue(expression);
        return this.sourceValue(binding.bindingSource).method.refers( binding );
    };

    outputValue = (expression:string) => {
        const dict = {
            "undefined": () => "",
            "object": () => "",
            "boolean": () => "",
            "function": () => "",
            "symbol": () => "",
            "number": ( data:number ) => String(data),
            "string": ( data:string ) => data,
        }  as { [key:string]:Function };
        const binding = this.obtainValue(expression);
        binding.bindingValue = dict[typeof binding.bindingValue](binding.bindingValue);
        return binding;
    };

    recordValue = (expression:string, value:any) => {
        const binding = BindingParser.bindingParsingExpression(expression);
        // Проверка значения на возможное выражение или объект
        if(typeof value === "string"){
            // Проверка выражение строка или биндинг
            binding.bindingValue = this.obtainValue(value).bindingValue;
        } else {
            binding.bindingValue = value;
        }
        // Проверка на наличие конструирования
        new CoreConstruct().launch(binding);
        // Проверка на наличие конвертации
        new CoreConverter().launch(binding);
        this.sourceValue(binding.bindingSource).method.record( binding );
    };

    removeValue = (expressionInto:string, expressionData?:string) => {
        const binding = BindingParser.bindingParsingExpression(expressionInto);
        // Проверка на удаление по ключу, или объекта в ключе
        if(expressionData){
            const data = this.obtainValue(expressionData);
            binding.bindingValue = data.bindingValue;
        }
        this.sourceValue(binding.bindingSource).method.remove( binding );
    };
}