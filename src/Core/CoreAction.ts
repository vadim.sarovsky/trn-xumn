import {cloneElement, collectChildrenAttr, parseXML} from "./Util/NodeXML";
import {obtainCoreCommand} from "./CoreCommand";
import {conditionFacade} from "./CoreCondition";

export class CoreAction {
    static implement = (template:string, selector:string) =>{
        const element = parseXML(template).querySelectorAll( selector );
        collectChildrenAttr(template,selector).map( async(props:any,i:number) => {
            if(obtainCoreCommand(props.commandName)){
                props.template = cloneElement(element[i]);
                const perform = obtainCoreCommand(props.commandName).perform;
                return await perform(props);
            } else {
                return console.log("Incorrect commandName:",props.commandName);
            }
        });
    };

    static condition = (template:string, selector:string) =>{
        const condition = {status: undefined } as { [key:string]:undefined | boolean };
        collectChildrenAttr(template,selector).map( (props:any) => condition.status = conditionFacade(props) ? true : condition.status );
        return condition.status;
    }
}