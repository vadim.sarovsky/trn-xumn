import {useEffect} from "react";

interface useOutsideClickHandlerProps {
    elementReference: any,
    onOutsideClick: (data?: any) => void,
}


export const useOutsideClick = ({ elementReference, onOutsideClick }: useOutsideClickHandlerProps) => {
    useEffect(() => {
        const handleClick = (e: any) => {
            if (elementReference.current){
                if (elementReference.current.contains(e.target)) {
                    return;
                } else {
                    onOutsideClick();
                }
            } else {
                return
            }
        };
        document.addEventListener("mousedown", handleClick);
        return () => document.removeEventListener("mousedown", handleClick);
    }, [elementReference, onOutsideClick]);
};