import React, {FormEvent, FunctionComponent, useRef} from "react";
import {CoreBinding} from "../../CoreBinding";
import {observer, useLocalStore} from "mobx-react-lite";
import "./data-auto-complete-box.css";
import {ContentService} from "../../Service/Content/ContentService";
import AutoCompleteGroup from "./AutoCompleteGroup";
import AutoCompleteInput from "./AutoCompleteInput";
import {useOutsideClick} from "../Use/UseOutsideClick";

type comboBoxType = {
    placeholder: string;
    style?:string;
    into:string;
    query:string;
    filter:string;
    value:string;
    providerType:string;
    template:string;
}

export const AutoComplete: FunctionComponent<comboBoxType> = observer((props) => {
    const state = useLocalStore(() => ({
        expanded: false,
        value: new CoreBinding().outputValue(props.value).bindingValue
    }));

    const handleChange = async (e: FormEvent<HTMLInputElement>) => {
        state.expanded = false;
        state.value = e.currentTarget.value;
        new CoreBinding().recordValue(props.query,state.value);
        if (state.value.length > 2) {
            await new ContentService().supplyContent(props);
            state.expanded = true;
        }
    };

    const toggleExpanded = () => {
        state.value = new CoreBinding().outputValue(props.value).bindingValue;
        state.expanded = false;
    };

    const removeValue = () => {
        new CoreBinding().removeValue(props.into);
        new CoreBinding().removeValue(props.query);
        new CoreBinding().removeValue(props.value);
        new CoreBinding().removeValue(props.filter);
        state.expanded = false;
        state.value = "";
    };

    const elementReference = useRef(null);

    useOutsideClick({elementReference,onOutsideClick:toggleExpanded});

    return (
        <div data-component={"AutoComplete"} aria-expanded={state.expanded} ref={elementReference}>
            <AutoCompleteInput value={state.value} onChange={handleChange} placeholder={props.placeholder} removeValue={removeValue}/>
            <AutoCompleteGroup setExpanded={toggleExpanded} children={props.children}/>
        </div>
    )
});

export default AutoComplete;