import React from "react";

const Icon = () =>
    <svg role="img" width="16" height="16" viewBox="0 0 16 16">
        <g id="point" strokeWidth="2" strokeLinecap="round">
            <line x1="4" y1="4" x2="8" y2="8"/>
            <line x1="8" y1="8" x2="12" y2="4"/>
        </g>
    </svg>;

export default Icon;