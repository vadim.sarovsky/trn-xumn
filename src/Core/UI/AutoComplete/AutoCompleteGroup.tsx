import React, {FunctionComponent} from "react";
import "./data-auto-complete-box.css";

type TAutoCompleteGroup = {
    setExpanded:Function;
}

export const AutoCompleteGroup: FunctionComponent<TAutoCompleteGroup> = (props) =>
    <div role="group"
         data-component={"AutoCompleteGroup"}
         onClick={()=>props.setExpanded(false)}
         children={props.children}/>;

export default AutoCompleteGroup;