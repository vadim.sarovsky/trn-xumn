import React, {FunctionComponent} from "react";
import "./data-auto-complete-box.css";
type TAutoCompleteInput = {
    value:string;
    onChange:Function;
    removeValue:Function;
    placeholder:string;
}

export const AutoCompleteInput: FunctionComponent<TAutoCompleteInput> = (props) =>
    <div role="presentation" data-component={"AutoCompleteInput"}>
        <input type="text"
               value={props.value}
               onChange={(e)=> props.onChange(e)}
               placeholder={props.placeholder}/>
        <button type="button"
                data-component={"AutoCompleteRemove"}
                onClick={()=>props.removeValue()}>
            <svg role="img" width="24" height="24" viewBox="0 0 24 24">
                <g id="point" strokeWidth="2" strokeLinecap="round">
                    <line x1="8" y1="8" x2="16" y2="16"/>
                    <line x1="8" y1="16" x2="16" y2="8"/>
                </g>
            </svg>
        </button>
    </div>;

export default AutoCompleteInput;

