import React, {FunctionComponent, Fragment} from "react";

const Template: FunctionComponent = (props) =>
    <Fragment>{props.children}</Fragment>;

export default Template;