export type TDateTime = {
    style?:string;
    value:string;
    locale:string;
    time?:"true";
}