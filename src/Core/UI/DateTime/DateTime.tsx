import React, {FunctionComponent} from 'react';
import {observer} from "mobx-react-lite";
import {CoreBinding} from "../../CoreBinding";
import {TDateTime} from "./TDateTime";

export const DateTime: FunctionComponent<TDateTime> = observer((props) =>{
    let value = String();
    const datetime = new CoreBinding().outputValue( props.value ).bindingValue;
    const options = {
        "year": "numeric",
        "month": "numeric",
        "day": "numeric"
    } as { [key:string]:string };
    if (props.time){
        options.hour = "2-digit";
        options.minute = "2-digit";
    }

    if(datetime){
        if (!isNaN(Date.parse(datetime))) {
            value = new Date(datetime).toLocaleString(props.locale, options);
        }
    }

    return <time dateTime={datetime} data-component="DateTime" data-style={props.style}>{value}</time>;

});

export default DateTime;