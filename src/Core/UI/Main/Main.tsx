import React, {FunctionComponent} from 'react';
import {TMain} from "./TMain";

export const Main: FunctionComponent<TMain> = (props) =>
    <main role="main"
          data-component="Main"
          data-style={props.style}
          children={props.children}/>;

export default Main;