export type TRoute ={
    pathname:string
    redirect?:string
    templateIdentifier:string;
    processMessage?:string;
}