import React, {FunctionComponent} from 'react';
import {RouterController} from "./RouterController";
import {TRouter} from "./TRouter";
import {useObserver} from "mobx-react-lite";
import {store} from "../../CoreStore";

export const Router: FunctionComponent<TRouter> = (props) =>{
    new RouterController( props.authentication, props.unknown, props.template );
    return useObserver(() => {
        return store.failure.method.status() ? null : <>{props.children}</>;
    })
};

export default Router;