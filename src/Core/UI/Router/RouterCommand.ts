
export const mutationHistory = () => {
    const event = new Event("MutationHistory", { "bubbles": true, "cancelable": false });
    document.dispatchEvent( event );
};

export const installHistory = (URI:string) => {
    window.history.pushState("","",URI);
    mutationHistory();
};

export const replaceHistory = (URI:string) => {
    window.history.replaceState("","",URI);
    mutationHistory();
};