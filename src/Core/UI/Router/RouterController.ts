import {mutationHistory, replaceHistory} from "./RouterCommand";
import {collectChildrenAttr} from "../../Util/NodeXML";
import {store} from "../../CoreStore";

export class RouterController {
    protected readonly authentication: string;
    protected readonly unknown: string;
    protected readonly route: Array <any>;
    protected readonly redirect: Array <any>;
    protected readonly overlook: Array <any>;
    protected readonly protection: Array <any>;


    constructor(authentication: string, unknown: string, template: string) {
        this.authentication = authentication;
        this.unknown = unknown;
        this.route = collectChildrenAttr(template, "Route" );
        this.redirect = collectChildrenAttr(template, "Redirect" );
        this.overlook= collectChildrenAttr(template, "Overlook");
        this.protection = collectChildrenAttr(template, "Protection Route");

        this.historyUpdate();
        window.addEventListener("MutationHistory", () => this.historyUpdate());
        window.addEventListener("SessionMutation", () => this.historyUpdate());
        window.addEventListener("popstate", () => mutationHistory());
    }
    protected historyUpdate() {
        const redirect = this.redirect.find(e => e.pathname === window.location.pathname);
        // Проверка на редирект
        if(redirect){
            replaceHistory(redirect.to);
        } else {
            // Агригация всех маршрутов из шаблона
            if (this.route.some(e => e.pathname === window.location.pathname)) {
                // Проверка авторизации
                if ( store.session.method.status()) {
                    // Замена пути если произошла авторизация и текущий путь соответсвует авторизации
                    if (window.location.pathname === this.authentication) {
                        replaceHistory("/");
                    }
                } else {
                    if (window.location.pathname !== this.authentication) {
                        // Замена пути если пользователь не авторизован и находится на защищёном маршрует
                        if (this.protection.some(e => e.pathname === window.location.pathname)) {
                            replaceHistory(this.authentication);
                        }
                    }
                }
            } else {
                // Замена пути если маршрут не существует и не игнорируемый
                if(!this.overlook.some(e => window.location.pathname.includes(e.pathname ))){
                    replaceHistory(this.unknown);
                }
            }
        }
    }
}