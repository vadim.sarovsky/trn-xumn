import React, {FunctionComponent} from 'react';
import {useObserver} from "mobx-react-lite";
import {store} from "../../CoreStore";

export const Protection: FunctionComponent = (props) =>
    useObserver(() =>
        store.session.method.status() ? <>{props.children}</> : null
    );

export default Protection;