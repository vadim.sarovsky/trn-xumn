import React, {FunctionComponent} from "react";
import {observer} from "mobx-react-lite";
import "./data-marker.css";
import {CoreBinding} from "../../CoreBinding";
import {TMarker} from "./TMarker";

export const Marker:FunctionComponent<TMarker> = observer((props) =>{
    let current:boolean= false;
    if (props.value){
        if(new CoreBinding().obtainValue(props.current).bindingValue !== undefined){
            current = new CoreBinding().obtainValue(props.current).bindingValue === new CoreBinding().obtainValue(props.value).bindingValue;
        }
    }
    return <mark role="presentation"
                 data-component="Marker"
                 data-style={props.style}
                 aria-current={current}
                 children={props.children}/>;
});

export default Marker;