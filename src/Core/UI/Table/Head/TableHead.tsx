import React,{FunctionComponent} from "react";
import {TTableHead} from "./TTableHead";

const TableHead: FunctionComponent<TTableHead> = (props) =>
    <div data-component="TableHead" data-style={props.style} children={props.children}/>;

export default TableHead;
