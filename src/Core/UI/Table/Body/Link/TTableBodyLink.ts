export type TTableBodyLink= {
    style?: string,
    valueType?:string,
    pathname:string,
    template:string,
}