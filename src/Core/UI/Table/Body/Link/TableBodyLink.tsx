import React, {FunctionComponent} from "react";
import {useObserver} from "mobx-react-lite";
import {linkGenerator} from "../../../../Util/LinkGenerator";
import {installHistory} from "../../../Router/RouterCommand";
import {TTableBodyLink} from "./TTableBodyLink";

const TableBodyLink: FunctionComponent<TTableBodyLink> = (props) => {
    const href = linkGenerator(props.template, props.pathname, props.valueType);
    return useObserver(() =>
        <div data-component="TableBody"
             data-style={props.style}
             onClick={()=>installHistory(href)}
             children={props.children}/>
    );
};

export default TableBodyLink;