export type TTableBody= {
    style?: string,
    valueType?:string,
    path?:string,
    template:string,
}