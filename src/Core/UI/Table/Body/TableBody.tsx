import React, {FunctionComponent} from "react";
import {TTableBody} from "./TTableBody";

const TableBody: FunctionComponent<TTableBody> = (props) =>
    <div data-component="TableBody" data-style={props.style} children={props.children}/>;

export default TableBody;
