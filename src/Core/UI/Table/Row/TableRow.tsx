import React,{FunctionComponent} from "react";
import {TTableRow} from "./TTableRow";

const TableRow: FunctionComponent<TTableRow> = (props) =>
    <div role="row" data-component="TableRow" data-style={props.style} children={props.children}/>

export default TableRow;