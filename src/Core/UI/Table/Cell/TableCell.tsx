import React,{FunctionComponent} from "react";
import {TTableCell} from "./TTableCell";

const TableCell: FunctionComponent<TTableCell> = (props) =>
    <div role="cell" data-component="TableCell" data-style={props.style} children={props.children}/>;

export default TableCell;