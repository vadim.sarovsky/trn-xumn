import React,{FunctionComponent} from "react";

type TableFootType = {
    className?: string,
}

const TableFoot: FunctionComponent<TableFootType> = (props) => {
    return <tfoot data-style={props.className}>{props.children}</tfoot>;
};

export default TableFoot;
