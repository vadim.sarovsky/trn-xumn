import React,{FunctionComponent} from "react";

type TableColumnHeaderType = {
    className?: string,
}

const TableColumnHeader: FunctionComponent<TableColumnHeaderType> = (props) => {
    return <th role={"columnheader"} data-style={props.className}>{props.children}</th>;
};

export default TableColumnHeader;