import React,{FunctionComponent} from "react";

type TTable = {
    style?: string,
}

const Table: FunctionComponent<TTable> = (props) =>
    <div role="table" data-component="Table" data-style={props.style} children={props.children}/>;

export default Table;