import React, {FunctionComponent} from "react";
import {installHistory} from "../Router/RouterCommand";
import {linkGenerator} from "../../Util/LinkGenerator";
import {useObserver} from "mobx-react-lite";

type TableBodyLinkType = {
    className?: string;
    style?:string;
    pathName:string;
    template:string;
    valueType?: string;

}

const TableBodyLink: FunctionComponent<TableBodyLinkType> = (props) => {
    const href = linkGenerator(props.template, props.pathName, props.valueType);
    return useObserver(() =>
        <tbody
            onClick={()=>installHistory(href)}
            data-style={props.className}
            children={props.children}/>

    )};

export default TableBodyLink;



