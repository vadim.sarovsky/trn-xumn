import React,{FunctionComponent} from "react";
import {useObserver} from "mobx-react-lite";
import {store} from "../../CoreStore";
import {TemplateProvider} from "../Provider/Template/TemplateProvider";
import {TModal} from "./TModal";



export const Modal:FunctionComponent<TModal> = (props) =>
    useObserver(() =>
        <dialog data-component="Modal" open={store.modal.method.status()}>
            <TemplateProvider templateIdentifier={store.modal.method.templateIdentifier()}/>
        </dialog>

    );
//             <TemplateProvider templateIdentifier={props.templateIdentifier}/>
//<Button command={props.command} className={"switch-dialog-modal"}/>


export default Modal;
