import React, {FormEvent, FunctionComponent} from 'react';
import {useObserver} from "mobx-react-lite";
import {CoreBinding} from "../../CoreBinding";
import {TTextArea} from "./TTextArea";

export const TextArea: FunctionComponent<TTextArea> = (props) =>
    useObserver(() =>
        <textarea
            data-component="TextArea"
            data-style={props.style}
            autoComplete={props.autoComplete}
            placeholder={props.placeholder}
            required={!!props.required}
            value={new CoreBinding().outputValue(props.value).bindingValue}
            onChange={(e: FormEvent<HTMLTextAreaElement>)=> new CoreBinding().recordValue( props.value, e.currentTarget.value)}
        />
    );

export default TextArea;