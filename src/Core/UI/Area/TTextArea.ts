export type TTextArea = {
    autoComplete?:string;
    style?:string;
    placeholder?:string;
    required?:"true";
    value:string
}