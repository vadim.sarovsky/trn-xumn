import React,{FunctionComponent} from "react";
import {TDecoration} from "./TDecoration";

const Decoration: FunctionComponent<TDecoration> = (props) =>
    <div role="presentation"
         data-component="Decoration"
         data-style={props.style}
         tabIndex={props.tabIndex}
         children={props.children}/>;

export default Decoration;