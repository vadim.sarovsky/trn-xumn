import React, {FunctionComponent} from 'react';
import {useObserver} from "mobx-react-lite";
import {CoreBinding} from "../../CoreBinding";
import {TText} from "./TText";

export const Text: FunctionComponent<TText> = (props) =>
    useObserver(() =>
        <output
            data-component="Text"
            data-style={props.style} children={new CoreBinding().outputValue(props.value).bindingValue}/>
    );

export default Text;