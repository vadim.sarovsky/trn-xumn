import React, {FunctionComponent} from "react";

type GridRowHeaderType = {
    className?: string;
}

const GridRowHeader:FunctionComponent<GridRowHeaderType> = (props) =>
    <div role="rowheader" data-style={props.className} children={props.children}/>;

export default GridRowHeader;