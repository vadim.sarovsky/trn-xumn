import React,{FunctionComponent} from "react";

type GridCellType = {
    className?: string,
}

const GridCell: FunctionComponent<GridCellType> = (props) =>
    <div role="gridcell" data-style={props.className} children={props.children}/>;

export default GridCell;