import React,{FunctionComponent} from "react";
import {TGrid} from "./TGrid";

export const Grid: FunctionComponent<TGrid> = (props) =>
    <div role="grid" data-component="Grid" data-style={props.style} children={props.children}/>;

export default Grid;