import React,{FunctionComponent} from "react";

type GridRowType = {
    className?: string,
}

const GridRow: FunctionComponent<GridRowType> = (props) =>
    <div role="row" data-style={props.className} children={props.children}/>;

export default GridRow;
