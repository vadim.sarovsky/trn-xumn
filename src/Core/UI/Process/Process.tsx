import React, {FunctionComponent} from "react";
import {useObserver} from "mobx-react-lite";
import {store} from "../../CoreStore";
import Dialog from "../Dialog/Dialog";

export const Process: FunctionComponent = (props) =>
    useObserver(() =>
        <Dialog open={store.process.method.status()} {...props} children={props.children}/>
    );

export default Process;