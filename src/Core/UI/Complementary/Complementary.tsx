import React, {FunctionComponent} from 'react';
import {TComplementary} from "./TComplementary";

const Complementary: FunctionComponent<TComplementary> = (props) =>
    <nav role="complementary" data-component="Complementary" data-style={props.style} children={props.children}/>;

export default Complementary;