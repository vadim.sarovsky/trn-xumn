import React, {FunctionComponent} from 'react';
import {TTextBlock} from "./TTextBlock";

export const TextBlock: FunctionComponent<TTextBlock> = (props) =>
    <div data-component="TextBlock" data-style={props.style} children={props.children}/>;

export default TextBlock;