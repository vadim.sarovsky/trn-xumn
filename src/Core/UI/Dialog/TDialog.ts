export type TDialog = {
    open?: boolean,
    style?: string
}