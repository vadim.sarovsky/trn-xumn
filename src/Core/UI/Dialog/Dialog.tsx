import React, {FunctionComponent} from 'react';
import {useObserver} from "mobx-react-lite";
import {TDialog} from "./TDialog";

export const Modal: FunctionComponent<TDialog> = (props) =>
    useObserver(() =>
        <dialog data-component="Dialog" data-style={props.style} open={props.open} children={props.children}/>
    );

export default Modal