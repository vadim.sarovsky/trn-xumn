import React, {FunctionComponent} from 'react';
import {useLocalStore, useObserver} from "mobx-react-lite";
import ExpanderGroup from "./ExpanderGroup";
import Icon from "./Icon";
import {TExpander} from "./TExpander";
import "./expander.css";

const Expander: FunctionComponent<TExpander> = (props) => {
    const expanded = useLocalStore(() => ({ state: props.expanded === "true" }));
    return useObserver(() =>
        <div role="tablist" data-component="Expander" className="expander" aria-expanded={expanded.state}>
            <div role="tab" onClick={()=>(expanded.state = !expanded.state)}>
                <div role="term">{props.value}</div>
                <div role="presentation"><Icon/></div>
            </div>
            <ExpanderGroup open={expanded.state} children={props.children}/>
        </div>
    )
};

export default Expander;