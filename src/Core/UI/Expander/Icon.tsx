import React from "react";

const Icon = () =>
    <svg role="img" width="12" height="16" viewBox="0 0 12 16">
        <g id="arrow" strokeWidth="1" strokeLinecap="round" fill="none">
            <line x1="4" y1="4" x2="8" y2="8"/>
            <line x1="4" y1="12" x2="8" y2="8"/>
        </g>
    </svg>;

export default Icon;