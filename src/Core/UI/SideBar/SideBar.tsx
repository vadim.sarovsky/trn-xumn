import React, {FunctionComponent} from 'react';

const SideBar: FunctionComponent = (props) =>
    <aside role="directory" data-component="SideBar" children={props.children}/>;

export default SideBar;
