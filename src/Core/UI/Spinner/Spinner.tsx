import React from "react";

const Spinner = () =>
    <svg width="64" height="64" viewBox="0 0 64 64" xmlns="http://www.w3.org/2000/svg">
        <g>
            <animateTransform attributeName="transform" type="rotate" values="0 32 32;256 32 32" begin="0s" dur="1.28s" fill="freeze" repeatCount="indefinite"/>
            <circle fill="none" strokeWidth="4" strokeLinecap="round" cx="32" cy="32" r="24" strokeDasharray="192" strokeDashoffset="640">
                <animate attributeName="stroke" values="#48F;#D33;#FC2;#195;#48F" begin="0s" dur="5.6s" fill="freeze" repeatCount="indefinite"/>
                <animateTransform attributeName="transform" type="rotate" values="0 32 32;128 32 32;480 32 32" begin="0s" dur="1.28s" fill="freeze" repeatCount="indefinite"/>
                <animate attributeName="stroke-dashoffset" values="192;48.96;192" begin="0s" dur="1.28s" fill="freeze" repeatCount="indefinite"/>
            </circle>
        </g>
    </svg>;

export default Spinner;