export type TCondition = {
    conditionType:string;
    value:string;
    source:string;
    valueType:string;
}