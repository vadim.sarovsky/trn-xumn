import React, {FunctionComponent} from "react";
import {useObserver} from "mobx-react-lite";
import {conditionFacade} from "../../CoreCondition";
import {TCondition} from "./TCondition";

export const Condition: FunctionComponent<TCondition> = (props) =>
    useObserver(() =>
        conditionFacade(props) ? <>{props.children}</> : null
    );

export default Condition;