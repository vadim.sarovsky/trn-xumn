import React, {FunctionComponent} from 'react';
import {TFooter} from "./TFooter";

export const Footer: FunctionComponent<TFooter> = (props) =>
    <footer role="contentinfo" data-style={props.style} children={props.children}/>;

export default Footer;
