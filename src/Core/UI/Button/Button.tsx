import React, {FunctionComponent} from 'react';
import {useObserver} from "mobx-react-lite";
import {CoreAction} from "../../CoreAction";
import {TButton} from "./TButton";

const Button: FunctionComponent<TButton> = (props) =>
    useObserver(() => {
            return (
                <button data-component="Button"
                        type={"button"}
                        disabled={CoreAction.condition(props.template,":root > Disable")}
                        aria-current={props.current}
                        data-style={props.style}
                        onClick={ async () => CoreAction.implement(props.template,":root > Command") }
                        children={props.children}/>
            )
        }
    );

export default Button;

