import React, {FunctionComponent, useState} from "react";
import "./range.css";
import {useObserver} from "mobx-react-lite";
import {CoreBinding} from "../../../CoreBinding";

type TRange = {
    value:string
}
const Range: FunctionComponent<TRange> = (props) => {
    const [expanded, setExpanded] = useState(false);
    return useObserver(() =>
        <div data-component="PaginatorRange" onClick={()=>setExpanded(!expanded)} aria-expanded={expanded}>
            <div data-component="PaginatorRangeGroup" role="group" children={props.children}/>
            <div data-component="PaginatorRangeState" role="group">
                <output children={new CoreBinding().outputValue(props.value).bindingValue}/>
                <div role="presentation">
                    <svg role="img" width="16" height="12" viewBox="0 0 16 12">
                        <g id="arrow" strokeWidth="1.25" strokeLinecap="round" fill="none">
                            <line x1="4" y1="4" x2="8" y2="8"/>
                            <line x1="12" y1="4" x2="8" y2="8"/>
                        </g>
                    </svg>
                </div>
            </div>
        </div>
    )
};

export default Range;