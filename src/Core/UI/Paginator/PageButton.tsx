import React, {FunctionComponent} from "react";
import {obtainCoreCommand} from "../../CoreCommand";

type TPageButton = {
    value:number;
    current?:"page";
    command:string;
    param:string;
}

const PageButton: FunctionComponent<TPageButton> = (props) =>{
    const clickHandler = async () =>{
        const command = obtainCoreCommand(props.command);
        if(command){
            const perform = command.perform;
            await perform(props);
        } else {
            console.log("Incorrect command:",props.command);
        }
    };
    const value = (i:number) => ++i;
    return <button type="button"
                   data-component="PageButton"
                   aria-current={props.current}
                   onClick={clickHandler}
                   children={value(props.value)}/>
};

export default PageButton;