import React, {FunctionComponent} from 'react';
import {ResourceFactory} from "../../Factory/ResourceFactory";
import {TResource} from "./TResource";

const Resource: FunctionComponent<TResource> = (props) =>
    <style type="text/css" children={new ResourceFactory().launch({template:props.template})}/>;

export default Resource;