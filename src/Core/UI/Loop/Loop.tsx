import {createElement, FunctionComponent} from "react";
import {useObserver} from "mobx-react-lite";
import {ComponentFactory} from "../../Factory/ComponentFactory";
import {BindingParser} from "../../Util/BindingParser";
import {CoreBinding} from "../../CoreBinding";

import {
    parseXML,
    cloneDocumentElement,
    replaceRootElement,
    recordElementValueAttr
} from "../../Util/NodeXML";

import {TLoop} from "./TLoop";


export const Loop: FunctionComponent<TLoop> = (props) =>
    useObserver(() => {
            let pattern: string | null = null;
            const expression = BindingParser.bindingParsingExpression(props.value);
            const bindingSource = expression.bindingSource;
            const bindingPath = expression.bindingPath;
            if(bindingSource) {
                if (bindingPath) {
                    const data = new CoreBinding().obtainValue(props.value).bindingValue;
                    if (Array.isArray(data)) {
                        const root = parseXML("<Template/>");
                        const fragment = replaceRootElement(props.template,"<Template/>");
                        if (fragment) {
                            let i = data.length;
                            while (i--) {
                                const element = cloneDocumentElement(fragment);
                                recordElementValueAttr(bindingSource, bindingPath, i, element);
                                root.documentElement.prepend(element);
                            }
                            pattern = new XMLSerializer().serializeToString(root);
                        }
                    }
                }
            }
            return pattern ? createElement( ComponentFactory, {template: pattern}) : null;
        }
    );

export default Loop;