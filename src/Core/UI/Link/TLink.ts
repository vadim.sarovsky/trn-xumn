
export type TLink = {
    style?:string;
    pathname:string;
    template:string;
    valueType?: string;
}