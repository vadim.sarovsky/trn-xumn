import React, {FunctionComponent} from 'react'
import {useObserver} from "mobx-react-lite";
import {installHistory} from "../Router/RouterCommand";
import {linkGenerator} from "../../Util/LinkGenerator";
import {TLink} from "./TLink";

const Link: FunctionComponent<TLink> = (props) => {
    const href = linkGenerator(props.template, props.pathname, props.valueType);
    return useObserver(() =>
        <button type="button"
                role="link"
                data-component="Button"
                data-style={props.style}
                aria-current={window.location.href.includes(href) ? "page" : false}
                onClick={()=>installHistory(href)}
                children={props.children}/>
    )
};

export default Link;
