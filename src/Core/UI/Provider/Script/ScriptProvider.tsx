import  {FunctionComponent, createElement, useEffect, useState, Fragment} from "react";
import {contentService} from "../../../Service/Content/ContentService";
import {TScriptProvider} from "./TScriptProvider";

export const ScriptProvider:FunctionComponent<TScriptProvider> = (props) => {
/*
    async function getComponent() {
        const {default} = await import('./my-module');
        return React.createElement(default.view)
    })

 */

    const [state, setState] = useState(false);
    const service = contentService;
    useEffect(() => {service.supplyContent(props).then(() => {setState(service.state)})}, [service,props]);
    return state ? createElement(Fragment, {children:props.children}) : null;
};

export default ScriptProvider;