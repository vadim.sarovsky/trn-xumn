import  {FunctionComponent, createElement, useEffect, useState, Fragment} from "react";
import {contentService} from "../../../Service/Content/ContentService";
import {TContentProvider} from "./TContentProvider";

export const ContentProvider:FunctionComponent<TContentProvider> = (props) => {
    const [state, setState] = useState(false);
    const service = contentService;
    useEffect(() => {service.supplyContent(props).then(() => {setState(service.state)})}, [service,props]);
    return state ? createElement(Fragment, {children:props.children}) : null;
};

export default ContentProvider;