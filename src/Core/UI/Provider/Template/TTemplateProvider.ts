export type TTemplateProvider = {
    templateIdentifier:string;
    renovate?:string;
    processNotice?:string;
}