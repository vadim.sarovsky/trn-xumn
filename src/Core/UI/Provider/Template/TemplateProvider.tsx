import {createElement, useEffect, useState} from "react";
import {observer} from "mobx-react-lite";
import {store} from "../../../CoreStore";
import {ComponentFactory} from "../../../Factory/ComponentFactory";
import {TTemplateProvider} from "./TTemplateProvider";
import {TemplateService} from "../../../Service/Template/TemplateService";

export const TemplateProvider = observer((props: TTemplateProvider) => {
    const [status, setStatus] = useState<"process" | "success" | "failure">("process");
    const service = new TemplateService();
    useEffect(() => {
        service.launch(props).then( () => setStatus(service.status));
    }, [ service, props ]);
    if(status === "success"){
        const template = store.template.method.obtain(props.templateIdentifier);
        return template ? createElement( ComponentFactory, {template: template }) : null;
    } else {
        return null
    }
});