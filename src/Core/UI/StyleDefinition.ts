type TStyleDictionary = {
    [key:string]:{
        [key:string]:string;
    };
};
type TTriggerDefinition = {
    [key:string]:string;
};

export const triggerDefinition:TTriggerDefinition = {
    Hover: ":hover",
    Disabled: "[disabled]",
    DisabledHover: "[disabled]:hover",
    CurrentPage:"[aria-current='page']",
};


export const styleProperty:TTriggerDefinition = {
    Display:"display",

    GridTemplateColumns:"grid-template-columns",
    GridTemplateRows:"grid-template-rows",
    Gap:"gap",
    GridRowGap:"grid-row-gap",
    GridColumnGap: "grid-column-gap",

    FlexWrap:"flex-wrap",
    AlignItems:"align-items",
    JustifyContent:"justify-content",
    FlexGrow:"flex-grow",

    Position:"position",
    Left:"left",
    Right:"right",
    Top:"top",
    Bottom:"bottom",
    Index:"z-index",

    Width:"width",
    MinWidth:"min-width",
    MaxWidth:"max-width",

    Height:"height",
    MinHeight:"min-height",
    MaxHeight:"max-height",

    Padding: "padding",
    Margin:"margin",

    BackgroundColor:"background-color",
    Background:"background",

    Border:"border",
    BorderLeft:"border-left",
    BorderRight:"border-right",
    BorderTop:"border-top",
    BorderBottom:"border-bottom",
    BackdropFilter:"backdrop-filter",
    WebkitBackdropFilter:"-webkit-backdrop-filter",


    BorderWidth:"border-width",
    BorderStyle:"border-style",
    BorderColor:"border-color",
    BorderRadius:"border-radius",

    Outline:"outline",

    BoxShadow:"box-shadow",

    FontFamily:"font-family",
    FontSize:"font-size",
    LineHeight:"line-height",
    FontWeight:"font-weight",
    TextAlign:"text-align",
    TextTransform:"text-transform",
    Color:"color",

    UserSelect:"user-select",
    PointerEvents:"pointer-events",

    Transition:"transition",
    Stroke:"stroke",
    Fill:"fill",
    StrokeDasharray:"stroke-dasharray",
    StrokeAlignment:"stroke-alignment",
    Overflow:"overflow",
    TextOverflow:"text-overflow",
    WhiteSpace:"white-space",
    Cursor:"cursor",
    Resize:"resize"
};

export const styleDictionary:TStyleDictionary = {
    Dialog:{
        AlignItems:styleProperty.AlignItems,
        JustifyContent:styleProperty.JustifyContent,
        BackgroundColor:styleProperty.BackgroundColor,
    },
    Main: {
        Padding: styleProperty.Padding,
    },
    Decoration:{
        Display:styleProperty.Display,
        Width:styleProperty.Width,
        MinWidth:styleProperty.MinWidth,
        MaxWidth:styleProperty.MaxWidth,
        Height:styleProperty.Height,
        MinHeight:styleProperty.MinHeight,
        MaxHeight:styleProperty.MaxHeight,
        Padding:styleProperty.Padding,
        Margin:styleProperty.Margin,
        BackgroundColor:styleProperty.BackgroundColor,
        Background:styleProperty.Background,

        Border:styleProperty.Border,
        BorderLeft:styleProperty.BorderLeft,
        BorderRight:styleProperty.BorderRight,

        BorderTop:styleProperty.BorderTop,
        BorderBottom:styleProperty.BorderBottom,
        BorderRadius:styleProperty.BorderRadius,

        Outline:styleProperty.Outline,

        BackdropFilter:styleProperty.BackdropFilter,
        WebkitBackdropFilter:styleProperty.WebkitBackdropFilter,

        Overflow:styleProperty.Overflow,

        Index:styleProperty.Index,


        FlexGrow:styleProperty.FlexGrow,


    },

    Padding:{
        Padding:styleProperty.Padding,
    },
    Margin:{
        Margin:styleProperty.Margin,
    },
    Border:{
        Border:styleProperty.Border,
        BorderTop:styleProperty.BorderTop,
        BorderBottom:styleProperty.BorderBottom,
        BorderWidth:styleProperty.BorderWidth,
        BorderStyle:styleProperty.BorderStyle,
        BorderColor:styleProperty.BorderColor,
        BorderRadius:styleProperty.BorderRadius,
    },
    Page:{
        Display:styleProperty.Display,
        AlignItems:styleProperty.AlignItems,
        JustifyContent:styleProperty.JustifyContent,
    },
    Table:{
        Width:styleProperty.Width,
        MinWidth:styleProperty.MinWidth,
        MaxWidth:styleProperty.MaxWidth,
    },
    TableHead:{
        BackgroundColor:styleProperty.BackgroundColor,
        BorderTop:styleProperty.BorderTop,
        BorderBottom:styleProperty.BorderBottom,
    },
    TableBody:{
        BackgroundColor:styleProperty.BackgroundColor,
        BorderTop:styleProperty.BorderTop,
        BorderBottom:styleProperty.BorderBottom,
    },
    TableBodyLink:{
        BackgroundColor:styleProperty.BackgroundColor,
        BorderTop:styleProperty.BorderTop,
        BorderBottom:styleProperty.BorderBottom,
        Cursor:styleProperty.Cursor,
    },
    TableRow:{
        BorderTop:styleProperty.BorderTop,
        BorderBottom:styleProperty.BorderBottom,
    },
    TableCell:{
        Padding:styleProperty.Padding,
        BackgroundColor:styleProperty.BackgroundColor,
    },

    Grid:{
        GridTemplateColumns:styleProperty.GridTemplateColumns,
        GridTemplateRows:styleProperty.GridTemplateRows,
        AlignItems:styleProperty.AlignItems,
        Gap:styleProperty.Gap,
        GridRowGap:styleProperty.GridRowGap,
        GridColumnGap:styleProperty.GridColumnGap,

        Width:styleProperty.Width,
        Height:styleProperty.Height,
    },


    PositionLayout: {
        "Width":styleProperty.Width,
        "Height":styleProperty.Height,
        "Margin":styleProperty.Margin,
        "Position": styleProperty.Position,
        "Left": styleProperty.Left,
        "Right": styleProperty.Right,
        "Top": styleProperty.Top,
        "Bottom": styleProperty.Bottom,
        "Index": styleProperty.Index,

    },
    Navigation: {
        Position: styleProperty.Position,
        Left: styleProperty.Left,
        Right: styleProperty.Right,
        Top: styleProperty.Top,
        Bottom: styleProperty.Bottom,
        Index: styleProperty.Index,
    },

    Complementary: {
        Position: styleProperty.Position,
        Left: styleProperty.Left,
        Right: styleProperty.Right,
        Top: styleProperty.Top,
        Bottom: styleProperty.Bottom,
        Index: styleProperty.Index,
    },


    StackLayout: {
        GridTemplateColumns: styleProperty.GridTemplateColumns,
        GridTemplateRows: styleProperty.GridTemplateRows,
        GridAutoFlow: "grid-auto-flow",
        AlignItems:styleProperty.AlignItems,
        Gap:styleProperty.Gap,
        Width:styleProperty.Width,
        Height:styleProperty.Height,
    },
    FlexLayout:{
        Width:styleProperty.Width,
        MinWidth:styleProperty.MinWidth,
        MaxWidth:styleProperty.MaxWidth,
        Height:styleProperty.Height,
        MinHeight:styleProperty.MinHeight,
        MaxHeight:styleProperty.MaxHeight,
        FlexWrap:styleProperty.FlexWrap,
        AlignItems:styleProperty.AlignItems,
        JustifyContent:styleProperty.JustifyContent,
        FlexGrow:styleProperty.FlexGrow,

    },
    InputText:{
        Height:styleProperty.Height,
        BackgroundColor:styleProperty.BackgroundColor,
        Padding:styleProperty.Padding,
        Border:styleProperty.Border,
        BorderColor:styleProperty.BorderColor,
        BorderRadius:styleProperty.BorderRadius,

        FontFamily:styleProperty.FontFamily,
        FontSize:styleProperty.FontSize,
        LineHeight:styleProperty.LineHeight,
        FontWeight:styleProperty.FontWeight,
        Color:styleProperty.Color,

        PointerEvents:styleProperty.PointerEvents,
    },
    Password:{
        Height:styleProperty.Height,
        BackgroundColor:styleProperty.BackgroundColor,
        Padding:styleProperty.Padding,
        Border:styleProperty.Border,
        BorderColor:styleProperty.BorderColor,
        BorderRadius:styleProperty.BorderRadius,

        FontFamily:styleProperty.FontFamily,
        FontSize:styleProperty.FontSize,
        LineHeight:styleProperty.LineHeight,
        FontWeight:styleProperty.FontWeight,
        Color:styleProperty.Color,

        PointerEvents:styleProperty.PointerEvents,
    },
    Button:{
        Width:styleProperty.Width,
        MinWidth:styleProperty.MinWidth,
        MaxWidth:styleProperty.MaxWidth,
        Height:styleProperty.Height,
        Margin:styleProperty.Margin,
        Padding:styleProperty.Padding,
        BackgroundColor:styleProperty.BackgroundColor,
        Border:styleProperty.Border,
        BorderColor:styleProperty.BorderColor,
        BorderRadius:styleProperty.BorderRadius,
        PointerEvents:styleProperty.PointerEvents,
        Color:styleProperty.Color,
        Stroke:styleProperty.Stroke,
        Fill:styleProperty.Fill,
        FontFamily:styleProperty.FontFamily,
        FontSize:styleProperty.FontSize,
        LineHeight:styleProperty.LineHeight,
        FontWeight:styleProperty.FontWeight,
        TextAlign:styleProperty.TextAlign,
        TextTransform:styleProperty.TextTransform,
        Overflow:styleProperty.Overflow,
        TextOverflow:styleProperty.TextOverflow,
        WhiteSpace:styleProperty.WhiteSpace,
        AlignItems:styleProperty.AlignItems,
        JustifyContent:styleProperty.JustifyContent,
        UserSelect:styleProperty.UserSelect,
        Index:styleProperty.Index,

    },
    Link:{
        Width:styleProperty.Width,
        MinWidth:styleProperty.MinWidth,
        MaxWidth:styleProperty.MaxWidth,
        Height:styleProperty.Height,
        Margin:styleProperty.Margin,
        Padding:styleProperty.Padding,
        BackgroundColor:styleProperty.BackgroundColor,
        Border:styleProperty.Border,
        BorderColor:styleProperty.BorderColor,
        BorderRadius:styleProperty.BorderRadius,
        PointerEvents:styleProperty.PointerEvents,
        Color:styleProperty.Color,
        Stroke:styleProperty.Stroke,
        Fill:styleProperty.Fill,
        FontFamily:styleProperty.FontFamily,
        FontSize:styleProperty.FontSize,
        LineHeight:styleProperty.LineHeight,
        FontWeight:styleProperty.FontWeight,
        TextAlign:styleProperty.TextAlign,
        TextTransform:styleProperty.TextTransform,
        Overflow:styleProperty.Overflow,
        TextOverflow:styleProperty.TextOverflow,
        WhiteSpace:styleProperty.WhiteSpace,
        AlignItems:styleProperty.AlignItems,
        JustifyContent:styleProperty.JustifyContent,
        UserSelect:styleProperty.UserSelect,
        Index:styleProperty.Index,
    },
    Text:{
        FontFamily:styleProperty.FontFamily,
        FontSize:styleProperty.FontSize,
        LineHeight:styleProperty.LineHeight,
        FontWeight:styleProperty.FontWeight,
        TextAlign:styleProperty.TextAlign,
        TextTransform:styleProperty.TextTransform,
        Color:styleProperty.Color,
        UserSelect:styleProperty.UserSelect,
        PointerEvents:styleProperty.PointerEvents,
        Overflow:styleProperty.Overflow,
        TextOverflow:styleProperty.TextOverflow,
        WhiteSpace:styleProperty.WhiteSpace,
    },

    DateTime:{
        FontFamily:styleProperty.FontFamily,
        FontSize:styleProperty.FontSize,
        LineHeight:styleProperty.LineHeight,
        FontWeight:styleProperty.FontWeight,
        TextAlign:styleProperty.TextAlign,
        TextTransform:styleProperty.TextTransform,
        Color:styleProperty.Color,
        UserSelect:styleProperty.UserSelect,
        PointerEvents:styleProperty.PointerEvents,
        Overflow:styleProperty.Overflow,
        TextOverflow:styleProperty.TextOverflow,
        WhiteSpace:styleProperty.WhiteSpace,
    },
    Image:{
        Width:styleProperty.Width,
        MinWidth:styleProperty.MinWidth,
        MaxWidth:styleProperty.MaxWidth,
        Height:styleProperty.Height,
        MinHeight:styleProperty.MinHeight,
        MaxHeight:styleProperty.MaxHeight,
        Margin:styleProperty.Margin,
        Padding:styleProperty.Padding,
        Stroke:styleProperty.Stroke,
        Fill:styleProperty.Fill,
        StrokeDasharray:styleProperty.StrokeDasharray,
        StrokeAlignment:styleProperty.StrokeAlignment,
    },
        "TextArea":{
            "Height":styleProperty.Height,
            "MinHeight":styleProperty.MinHeight,
            "MaxHeight":styleProperty.MaxHeight,
            "Resize":styleProperty.Resize,
        },


    "TextBlock":{
        "Display":styleProperty.Display,
        "FlexWrap":styleProperty.FlexWrap,
        "UserSelect":styleProperty.UserSelect,
        "PointerEvents":styleProperty.PointerEvents,
        "Overflow":styleProperty.Overflow,
        "TextOverflow":styleProperty.TextOverflow,
        "WhiteSpace":styleProperty.WhiteSpace,
    },

    "Status": {
        "Display": styleProperty.Display,
        "FontFamily": styleProperty.FontFamily,
        "FontSize": styleProperty.FontSize,
        "LineHeight": styleProperty.LineHeight,
        "TextAlign":styleProperty.TextAlign,
        "Width":styleProperty.Width,
        "Padding":styleProperty.Padding,
        "BorderRadius":styleProperty.BorderRadius,
    },
    "StatusCode":{
        "Color":styleProperty.Color,
        "BackgroundColor":styleProperty.BackgroundColor,
    },

};