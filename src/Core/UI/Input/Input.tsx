import React, {FormEvent, FunctionComponent} from 'react';
import {useObserver} from "mobx-react-lite";
import {CoreBinding} from "../../CoreBinding";
import {TInput} from "./TInput";

export const Input: FunctionComponent<TInput> = (props) =>
    useObserver(() =>
        <input
            data-style={props.style}
            autoComplete={props.autoComplete}
            type={props.inputType}
            placeholder={props.placeholder}
            required={!!props.required}
            value={new CoreBinding().outputValue(props.value).bindingValue}
            onChange={(e: FormEvent<HTMLInputElement>)=> new CoreBinding().recordValue( props.value, e.currentTarget.value)}
        />
    );

export default Input;