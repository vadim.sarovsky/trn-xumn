export type TInput = {
    inputType: string;
    autoComplete?:string;
    style?:string;
    className?:string;
    placeholder?:string;
    required?:"true";
    value:string
}