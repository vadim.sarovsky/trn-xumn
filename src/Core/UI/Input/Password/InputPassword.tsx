import React from 'react';
import Input from "../Input";
import {useObserver, useLocalStore} from "mobx-react-lite";
import {TInput} from "../TInput";

export const InputPassword = (props:TInput) => {
    const type = useLocalStore(() => ({ state: 'password' }));
    return useObserver(() =>
        <div data-component="Password"
             data-state={type.state}>
            <Input {...props} inputType={type.state}/>
            <button type={"button"} onClick={()=>(type.state = type.state === "password" ? "text" : "password")}>
                <svg role="img" width="32" height="32" viewBox="0 0 32 32">
                    <symbol id="password-part">
                        <circle cx="16" cy="16" r="8"/>
                    </symbol>
                    <g strokeWidth="2" strokeLinecap="round" fill="none">
                        <circle cx="16" cy="16" r="4"/>
                        <use href="#password-part" height="16" width="100%"/>
                    </g>
                </svg>
            </button>
        </div>
    );
};

export default InputPassword;