import React from 'react';
import Input from "../Input";
import {TInput} from "../TInput";

export const InputText = (props:TInput) => <Input {...props} inputType={"text"}/>;

export default InputText;