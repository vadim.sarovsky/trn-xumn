import React, {FunctionComponent} from 'react';
import {useObserver} from "mobx-react-lite";
import {store} from "../../CoreStore";

export const Router: FunctionComponent = (props) =>{
    return useObserver(() => {
        return store.failure.method.status() ? <>{props.children}</> : null;
    })
};

export default Router;