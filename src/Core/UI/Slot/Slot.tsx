import {createElement} from 'react';
import {TemplateProvider} from "../Provider/Template/TemplateProvider";
import {TSlot} from "./TSlot";

export const Slot = (props:TSlot) => createElement(TemplateProvider, {...props });

export default Slot;