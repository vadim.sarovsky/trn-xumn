import React, {FunctionComponent} from 'react';
import {TNavigation} from "./TNavigation";

const Navigation: FunctionComponent<TNavigation> = (props) =>
    <nav role="navigation" data-component="Navigation" data-style={props.style} children={props.children}/>;

export default Navigation;