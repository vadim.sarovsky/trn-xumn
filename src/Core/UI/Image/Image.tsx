import React,{FunctionComponent} from "react";
import {TImage} from "./TImage";

const Image: FunctionComponent<TImage> = (props) =>
    <>
        {props.useImg ? <svg role="img" data-component="Image" data-style={props.style}><use href={props.useImg}/></svg> : null}
        {props.srcImg ? <img alt={""} data-component="Image" data-style={props.style} src={props.srcImg}/> : null}
    </>;

export default Image;