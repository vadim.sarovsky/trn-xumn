import React, {FunctionComponent} from "react";
import {useObserver} from "mobx-react-lite";
import {store} from "../../CoreStore";
import Icon from "./Icon";
import "./sidebar-collapsible.css";

const SidebarCollapsible: FunctionComponent = (props) =>
    useObserver(() =>
        <div role="directory" className={"sidebar-collapsible"} aria-expanded={store.sidebar.method.status()}>
            <div role="group" children={props.children}/>
            <button type="button"
                    role="switch"
                    className={"sidebar-collapsible-switch"}
                    onClick={()=>store.sidebar.method.toggle()}
                    aria-checked={store.sidebar.method.status()}>

<Icon/>




            </button>
        </div>
    );

export default SidebarCollapsible;