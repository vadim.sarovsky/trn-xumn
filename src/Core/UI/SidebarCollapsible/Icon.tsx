import React from "react";

const Icon = () =>
    <svg role="img" width="32" height="32" viewBox="0 0 32 32">
        <g strokeWidth="1.25" strokeLinecap="round" fill="none">



            <line x1="24" y1="16" x2="16" y2="8"/>
            <line x1="24" y1="16" x2="16" y2="24"/>
            <line x1="16" y1="16" x2="8" y2="8"/>
            <line x1="16" y1="16" x2="8" y2="24"/>


        </g>
    </svg>;

export default Icon;


/*

            <line x1="8" y1="24" x2="24" y2="8"/>
            <line x1="8" y1="24" x2="16" y2="24"/>
            <line x1="8" y1="16" x2="8" y2="24"/>



            <line x1="24" y1="16" x2="16" y2="8"/>
            <line x1="24" y1="16" x2="16" y2="24"/>
            <line x1="16" y1="16" x2="8" y2="8"/>
            <line x1="16" y1="16" x2="8" y2="24"/>
 */