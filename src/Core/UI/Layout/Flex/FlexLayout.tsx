import React, {FunctionComponent} from 'react';
import {TFlexLayout} from "./TFlexLayout";

export const FlexLayout: FunctionComponent<TFlexLayout> = (props) =>
    <div role="presentation" data-component="FlexLayout" data-style={props.style} children={props.children}/>;

export default FlexLayout;