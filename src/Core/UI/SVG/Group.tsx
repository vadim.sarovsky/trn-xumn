import React,{FunctionComponent} from "react";

type TGroup ={
    strokeWidth?:string;
    fill?:string;
    strokeLinecap?:"butt" | "round" | "square";
    stroke?:string
}
const Group:FunctionComponent<TGroup> = (props) =>
    <g stroke={props.stroke}
       strokeWidth={props.strokeWidth}
       strokeLinecap={props.strokeLinecap}
       fill={props.fill}
       children={props.children}/>;

export default Group;