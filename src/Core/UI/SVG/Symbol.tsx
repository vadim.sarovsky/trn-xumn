import React,{FunctionComponent} from "react";

type TGroup ={
    id?:string;
    strokeWidth?:string;
    strokeLinecap?:"butt" | "round" | "square";
    strokeDasharray?:string;
    stroke?:string;
    fill?:string;
}
const Symbol:FunctionComponent<TGroup> = (props) =>
    <symbol id={props.id}
            stroke={props.stroke}
            strokeWidth={props.strokeWidth}
            strokeLinecap={props.strokeLinecap}
            strokeDasharray={props.strokeDasharray}
            fill={props.fill}
            children={props.children}/>;

export default Symbol;