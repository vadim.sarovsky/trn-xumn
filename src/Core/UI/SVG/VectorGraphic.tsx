import React,{FunctionComponent} from "react";

type TVectorGraphic={
    width:string;
    height:string;
    viewBox:string;
    hidden?:"false" | "true";
}
const VectorGraphic:FunctionComponent<TVectorGraphic> = (props) =>
    <svg role="img"
         data-component="VectorGraphic"
         aria-hidden={props.hidden}
         width={props.width}
         height={props.height}
         viewBox={props.viewBox}
         children={props.children}/>;

export default VectorGraphic;