import React,{FunctionComponent} from "react";

type TCircle={
    cx?:string;
    cy?:string;
    r?:string;
    stroke?:string;
    strokeWidth?:string;
    fill?:string;
}
const Circle:FunctionComponent<TCircle> = (props) =>
    <circle cx={props.cx}
            cy={props.cy}
            r={props.r}
            stroke={props.stroke}
            strokeWidth={props.strokeWidth}
            fill={props.fill}
            children={props.children}/>;

export default Circle;