import {action, observable} from "mobx";
import {TSessionParam} from "./TSessionParam";
import SessionStorage from "./Storage/SessionStorage";
import {obtainValue, recordValue, refersValue, removeValue} from "../../Util/ValueMethod";

export class SessionStore {
    @observable private state:boolean;
    @observable private props: TSessionParam;
    protected storage: SessionStorage;
    constructor() {
        this.state = false;
        this.props = {};
        this.storage = new SessionStorage("sessionStorage", "SessionMutation" );
        if(this.storage.obtain()){
            const data = JSON.parse(this.storage.obtain());
            this.state = data.state;
            this.props = data.props;
        }
    }

    @action private launch = () => {
        this.state = true;
        this.storage.record(JSON.stringify({state:this.state,props: this.props}));
    };

    private obtainValueByPath = (param:any) => obtainValue(this.props, param.bindingPath);
    private refersValueByPath = (param:any) => refersValue(this.props, param.bindingPath);
    @action private recordValueByPath = (param:any) => recordValue(this.props, param);
    @action private removeValueByPath = (param:any) => removeValue(this.props, param.bindingPath, param.bindingValue);
    @action private revoke = () => {
        this.state = false;
        this.props = {};
        this.storage.record(JSON.stringify({state:this.state,props: this.props}));
    };

    public method = {
        launch: () => this.launch(),
        status: () => this.state,
        obtain: (param:any) => this.obtainValueByPath(param),
        refers: (param:any) => this.refersValueByPath(param),
        record: (param:any) => this.recordValueByPath(param),
        remove: (param:any) => this.removeValueByPath(param),
        revoke: () => this.revoke(),
    }
}