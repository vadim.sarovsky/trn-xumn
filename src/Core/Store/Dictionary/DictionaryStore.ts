import {observable, action} from "mobx";
import {recordValue, removeValue, obtainValue, refersValue} from "../../Util/ValueMethod";

export class DictionaryStore {
    @observable private props: {
        [id: string]: any;
    };

    constructor() {
        this.props = {};
    }

    private obtainValueByPath = (param:any) => obtainValue(this.props, param.bindingPath);
    private refersValueByPath = (param:any) => refersValue(this.props, param.bindingPath);
    @action private recordValueByPath = (param:any) => recordValue(this.props, param);
    @action private removeValueByPath = (param:any) => removeValue(this.props, param.bindingPath, param.bindingValue);
    @action private revoke = () => this.props = {};

    public method = {
        obtain: (param:any) => this.obtainValueByPath(param),
        refers: (param:any) => this.refersValueByPath(param),
        record: (param:any) => this.recordValueByPath(param),
        remove: (param:any) => this.removeValueByPath(param),
        revoke: () => this.revoke(),
    }
}