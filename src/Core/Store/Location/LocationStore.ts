import {observable, action} from "mobx";
import {obtainValue, recordValue, refersValue, removeValue} from "../../Util/ValueMethod";


export class LocationStore {
    @observable pathname:string;
    @observable href:string;

    @observable props: {
        [id: string]: any;
    };

    constructor() {
        this.href = window.location.href;
        this.pathname = window.location.pathname;
        this.props = {};
        this.controller();
        window.addEventListener("MutationHistory", () => this.controller(), false);
    }

    private obtainValueByPath = (param:any) => obtainValue(this.props, param.bindingPath);
    private refersValueByPath = (param:any) => refersValue(this.props, param.bindingPath);
    @action private recordValueByPath = (param:any) => {recordValue(this.props, param)};
    @action private removeValueByPath = (param:any) => removeValue(this.props, param.bindingPath, param.bindingValue);

    @action protected controller = () => {
        this.props = {};
        this.props.status = false;
        this.href = window.location.href;
        this.pathname = window.location.pathname;
        const queryParams  = new URLSearchParams(window.location.search);
        for(let key of queryParams.keys()) {
            this.recordValueByPath({bindingPath:key,bindingValue:queryParams.get(key)});
        }
        this.props.status = true;
    };

    public method = {
        obtain: (param:any) => this.obtainValueByPath(param),
        refers: (param:any) => this.refersValueByPath(param),
        record: (param:any) => this.recordValueByPath(param),
        remove: (param:any) => this.removeValueByPath(param),
        revoke: () => null,
    }
}