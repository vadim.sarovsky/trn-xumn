import {TProcessStore} from "./TProcessStore"
import {action, observable} from "mobx";
import {obtainValue, recordValue, refersValue, removeValue} from "../../Util/ValueMethod";

export class ProcessStore {
    @observable public state: boolean;
    @observable private props: TProcessStore | any;
    constructor( ) {
        this.state = false;
        this.props = {};
    }

    @action public launch(param:TProcessStore){
        this.props = param;
        this.state = true;
        document.querySelector("body")?.setAttribute("data-process", "true");
    }

    @action public finish(){
        this.props = {};
        this.state = false;
        document.querySelector("body")?.removeAttribute("data-process");

    }

    private obtainValueByPath = (param:any) => obtainValue(this.props, param.bindingPath);
    private refersValueByPath = (param:any) => refersValue(this.props, param.bindingPath);
    @action private recordValueByPath = (param:any) => recordValue(this.props, param);
    @action private removeValueByPath = (param:any) => removeValue(this.props, param.bindingPath, param.bindingValue);

    public method = {
        status: () => this.state,
        launch: (param:TProcessStore) => this.launch(param),
        finish: () => this.finish(),
        obtain: (param:any) => this.obtainValueByPath(param),
        refers: (param:any) => this.refersValueByPath(param),
        record: (param:any) => this.recordValueByPath(param),
        remove: (param:any) => this.removeValueByPath(param),
    }
}