import {TTemplateStore} from "./TTemplateStore";
import {observable,action} from "mobx";

export class TemplateStore {
    @observable private readonly props: {
        [id: string]: string;
    };

    constructor() {
        this.props = {};
    }

    @action public recordByName = ( param:TTemplateStore ) => this.props[param.id] = param.template;

    public obtainByName = (id:string) => this.props[id] ? this.props[id] : undefined;

    public method = {
        obtain:(id:string) => this.obtainByName(id),
        record:(param:TTemplateStore) => this.recordByName(param)
    }
}