export type TTemplateStore = {
    id: string;
    template: string;
}