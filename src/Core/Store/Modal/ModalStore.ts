import {action, observable} from "mobx";
import {TModalStore} from "./TModalStore";

export class ModalStore {
    @observable private state: boolean;
    @observable private templateIdentifier: string;
    constructor( ) {
        this.templateIdentifier = "";
        this.state = false;
        window.addEventListener("MutationHistory", () => this.finish(), false);
    }

    @action public launch(props:TModalStore){
        this.templateIdentifier = props.templateIdentifier;
        this.state = true;
        document.querySelector("body")?.setAttribute("data-modal", "true");
    }

    @action public finish(){
        this.state = false;
        this.templateIdentifier = "";
        document.querySelector("body")?.removeAttribute("data-modal");
    }

    public method = {
        templateIdentifier: () => this.templateIdentifier,
        status: () => this.state,
        launch: (props:any) => this.launch(props),
        finish: () => this.finish(),
    }
}