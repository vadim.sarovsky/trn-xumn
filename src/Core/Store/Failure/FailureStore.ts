import {action, observable} from "mobx";
import {obtainValue, recordValue, refersValue, removeValue} from "../../Util/ValueMethod";
import {TFailureStore} from "./TFailureStore";

export class FailureStore {
    @observable private state: boolean;
    @observable private props: TFailureStore | any;
    constructor( ) {
        this.state = false;
        this.props = {};
    }
    @action public launch(props:any){
        this.props = props;
        this.state = true;
    }
    @action public finish(){
        this.props = {};
        this.state = false;
    }

    private obtainValueByPath = (param:any) => obtainValue(this.props, param.bindingPath);
    private refersValueByPath = (param:any) => refersValue(this.props, param.bindingPath);
    @action private recordValueByPath = (param:any) => recordValue(this.props, param);
    @action private removeValueByPath = (param:any) => removeValue(this.props, param.bindingPath, param.bindingValue);

    public method = {
        status: () => this.state,
        launch: (param:any) => this.launch(param),
        obtain: (param:any) => this.obtainValueByPath(param),
        refers: (param:any) => this.refersValueByPath(param),
        record: (param:any) => this.recordValueByPath(param),
        remove: (param:any) => this.removeValueByPath(param),
    }
}
