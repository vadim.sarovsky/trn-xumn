import {SessionStore} from "./Store/Session/SessionStore";
import {ContentStore} from "./Store/Content/ContentStore";
import {FailureStore} from "./Store/Failure/FailureStore";
import {ProcessStore} from "./Store/Process/ProcessStore";
import {SidebarStore} from "./Store/Sidebar/SidebarStore";
import {FiltersStore} from "./Store/Filters/FiltersStore";
import {TemplateStore} from "./Store/Template/TemplateStore";
import {LocationStore} from "./Store/Location/LocationStore";
import {ModalStore} from "./Store/Modal/ModalStore";
import {DictionaryStore} from "./Store/Dictionary/DictionaryStore";

export class CoreStore {
    readonly modal: ModalStore;

    readonly session: SessionStore;
    readonly process: ProcessStore;
    readonly failure: FailureStore;
    readonly sidebar: SidebarStore;
    readonly filters: FiltersStore;
    readonly content: ContentStore;
    readonly dictionary: DictionaryStore;
    readonly location: LocationStore;
    readonly template: TemplateStore;
    constructor() {
        this.modal = new ModalStore();
        this.session = new SessionStore();
        this.process = new ProcessStore();
        this.failure = new FailureStore();
        this.sidebar = new SidebarStore();
        this.filters = new FiltersStore();
        this.content = new ContentStore();
        this.dictionary = new DictionaryStore();
        this.location = new LocationStore();
        this.template = new TemplateStore();
    }
}

export const store:CoreStore = new CoreStore();