import {CommandService} from "../Service/Command/CommandService";
import {parseDocumentElement,cloneElement} from "../Util/NodeXML";
import {AppConfigs} from "../../App/AppConfigs";

const reviewStatus = async (response:any, props:any) => {
    if( response.status === 0 ){
        return response;
    } else {
        const sleep = (m:number) => new Promise(r => setTimeout(r, m));
        await sleep( 500 );
        return await actionStatus(props);
    }
};

export const actionStatus = async ( props:any ) => {
    const response = await new CommandService().launch(props);
    const result: any = await reviewStatus(response, props);
    return await result;
};

export const asyncDownload = async (props:any) => {
    let node:Element | null;
    const root = parseDocumentElement(props.template);
    node = root.querySelector(":root > RequestPerform");
    if(node) {
        await new CommandService().launch({...props,template:cloneElement(node)});
        node = root.querySelector(":root > RequestStatus");
        if(node){
            const requestActionStatus = await actionStatus({...props,template:cloneElement(node)});
            window.open( AppConfigs.API.origin + "/download/" + requestActionStatus.result.fileId, 'blank', 'toolbar=0,location=0,menubar=0');
        }
    }
};