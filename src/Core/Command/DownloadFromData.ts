import {CoreBinding} from "../CoreBinding";

export const downloadFromData = (props:any) => {
    const datetime = new Date();
    const date = datetime.getFullYear() + '-' +  datetime.getMonth() + '-' +  datetime.getDate();
    const time = datetime.getHours() + '-' + datetime.getMinutes() + '-' + datetime.getSeconds();
    const filename = date + '-' + time + '.cer';
    const element = document.createElement("a");
    const blob = new Blob([new CoreBinding().obtainValue(props.value).bindingValue], {type: "octet/stream"});
    const URI = window.URL.createObjectURL(blob);
    element.href = URI;
    element.download = filename;
    element.click();
    window.URL.revokeObjectURL( URI );
};