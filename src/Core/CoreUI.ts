export type TCoreUI = { [id: string]: any }
export const CoreUI = {
    "Template": require("./UI/Template/Template"),
    "Slot": require("./UI/Slot/Slot"),
    "Router": require("./UI/Router/Router"),
    "Protection": require("./UI/Router/Protection"),
    "Route": require("./UI/Router/Route/Route"),
    "Provider": require("./UI/Provider/Content/ContentProvider"),
    "Condition": require("./UI/Condition/Condition"),
    "Loop": require("./UI/Loop/Loop"),
    "Failure":require("./UI/Failure/Failure"),
    "Process": require("./UI/Process/Process"),
    "Modal": require("./UI/Modal/Modal"),
    "ScrollView": require("./UI/ScrollView/ScrollView"),
    "Spinner": require("./UI/Spinner/Spinner"),
    "Resource": require("./UI/Resource/Resource"),
    "TextArea": require("./UI/Area/TextArea"),

    "Text": require("./UI/Text/Text"),
    "TextBlock": require("./UI/TextBlock/TextBlock"),
    "DateTime": require("./UI/DateTime/DateTime"),
    "Image": require("./UI/Image/Image"),
    "Status": require("./UI/Status/Status"),
    "AutoRun": require("./UI/Button/AutoRun"),
    "Button": require("./UI/Button/Button"),
    "InputText": require("./UI/Input/Text/InputText"),
    "Password": require("./UI/Input/Password/InputPassword"),
    "CheckBox": require("./UI/CheckBox/CheckBox"),
    "AutoComplete": require("./UI/AutoComplete/AutoComplete"),
    "Link": require("./UI/Link/Link"),
    "Navigation": require("./UI/Navigation/Navigation"),
    "Complementary": require("./UI/Complementary/Complementary"),

    "Main": require("./UI/Main/Main"),
    "Page": require("./UI/Page/Page"),
    "Footer": require("./UI/Footer/Footer"),
    "Decoration": require("./UI/Decoration/Decoration"),
    "PositionLayout": require("./UI/Layout/Position/PositionLayout"),
    "StackLayout": require("./UI/Layout/Stack/StackLayout"),
    "FlexLayout": require("./UI/Layout/Flex/FlexLayout"),
    "Expander": require("./UI/Expander/Expander"),
    "SideBar": require("./UI/SideBar/SideBar"),
    "SidebarCollapsible": require("./UI/SidebarCollapsible/SidebarCollapsible"),
    "Grid": require("./UI/Grid/Grid"),
    "GridCell": require("./UI/Grid/GridCell"),
    "Table": require("./UI/Table/Table"),
    "TableHead": require("./UI/Table/Head/TableHead"),
    "TableBody": require("./UI/Table/Body/TableBody"),
    "TableBodyLink": require("./UI/Table/Body/Link/TableBodyLink"),
    "TableRow": require("./UI/Table/Row/TableRow"),
    "TableCell": require("./UI/Table/Cell/TableCell"),
    "Paginator": require("./UI/Paginator/Paginator"),
    "Range": require("./UI/Paginator/Range/Range"),

    "VectorGraphic": require("./UI/SVG/VectorGraphic"),
    "Symbol": require("./UI/SVG/Symbol"),
    "Use": require("./UI/SVG/Use"),
    "Group": require("./UI/SVG/Group"),
    "Line": require("./UI/SVG/Line"),
    "Circle": require("./UI/SVG/Circle"),
    "Rect": require("./UI/SVG/Rect"),

} as TCoreUI;

export const launchCoreUI = ( AppUI: TCoreUI ) => {
    for(const key in AppUI ){
        if(AppUI.hasOwnProperty(key)){
            CoreUI[key] = AppUI[key];
        }
    }
};